import { PrestationDataSet } from './../../models/prestation.model';

import { PrestationI } from "../../models/interface";
import { SavePrestationFormI } from "../../utils/customType";
import { isValidObjectId } from 'mongoose';
import { QuestionnaireSet } from '../../../questionnaireService/models/questionnaire.model';

export class PrestationController {

  static async test() {

    await this.save({
      nomPrestation: "Abonnement client",
      questionnaire: []
    });

    return Promise.resolve({
      error: false,
      status: 200,
      message: "admin service up",
      data: "version 1.0.0",
    });
  }



  static async save(prestation: PrestationI): Promise<SavePrestationFormI> {
    let newPrestation: SavePrestationFormI = await PrestationDataSet.save(prestation);
    return newPrestation;
  }

  static async getActivePrestations(): Promise<any>{
    return new Promise<any>(async(resolve, reject)=>{
        try{
            
            let allQuestionnaire = await PrestationDataSet.select({params:{status:1},excludes:null});
            console.log(allQuestionnaire);
            return resolve({error:false, status: 200, message:"la liste des questionnaires", data:allQuestionnaire});
        }//@ts-ignore
        catch(error){
            console.warn(error)
            return reject({error:true, status: 422, message:"une erreur interne s'est produite", data:null})

        }
    })
}

static async updatePrestationById(PrestationId : string, prestationData : PrestationI ) 
{
  return new Promise<any>(async (resolve, reject) => {
    try{
     
      let testId =isValidObjectId(PrestationId)
      let prestationUpdate;
      if(!testId) return resolve({
          error:true, 
          status:422, 
          message:"La prestation n'a été trouver.", 
          data:null});
          prestationUpdate = await PrestationDataSet.update({_id: PrestationId}, prestationData );
      return resolve({error:false, 
          status:200,
           message:"La prestation a été modifier.", 
          data:prestationUpdate});
    }
   catch (error)
   {
      console.warn(error);
      return reject({
        error: true,
        status: 500,
        message: "une erreur interne s'est produite",
        data: null,
      });
    }
  })
}

// Archivage des données
static async  archiveOnePrestation(PrestationId : string, prestationData : PrestationI):Promise<any>
  {
      return new Promise<any>(async(resolve, reject)=>{
          try{
              let testId =isValidObjectId(PrestationId)
              let PrestationeArchive;
              if(!testId) return resolve({error:true, 
                  status:400, 
                  message:"La prestation n'a pas été trouvé.", 
                  data:null});
                  PrestationeArchive = await PrestationDataSet.update({_id:PrestationId}, prestationData);
              return resolve({error:false, 
                  status:200, message:"La prestation est archivé.", 
                  data:null});
          }//@ts-ignore
          catch(error){
              console.warn(error)
              return reject({error:true, 
                  status: 500,
                   message:"une erreur interne s'est produite", 
                  data:null})

          }
      })
  }

  static async register(prestationData: PrestationI): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      try {
        let saveResult: any = await this.save(prestationData);
        if (saveResult.error)
          return resolve({
            error: true,
            status: 500,
            message: "Une erreur interne",
            data: saveResult,
          });
        saveResult.status = 201;
        return resolve(saveResult);
      } catch (error) {
        console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
      }
    });
  }

  static async getAll(): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
        try {
            let allPrestation : any= await PrestationDataSet.select({params:null, excludes:null});             
            for(const key  in allPrestation) {
              let questionnaireId = allPrestation[key].questionnaire[0]
              let questionnaire = await QuestionnaireSet.selectOne({_id :questionnaireId});
              allPrestation[key].questionnaire = questionnaire;
          } 
            return resolve({
              error: false,
              status: 200,
              message: "La liste des prestaions",
              data: allPrestation,
            });
          return resolve(allPrestation);
        } catch (error) {
            console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
        } 
    });

   
  }

  static async getOnePrestationById(prestationId: string): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      try{
        let onePrestation = await PrestationDataSet.selectOne({prestationId:prestationId});
        console.log(onePrestation);
        return resolve({
          error: false,
          status: 201,
          message: "La prestation recherchée",
          data: onePrestation,
        });
      } catch (error){
        console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
      }
    })
  }

  static async edit() {}

  static async delete() {}

  static async count() {}

}
