export interface SavePrestationFormI {
    error: boolean;
    data: any;
    message: string;
    status: number;
  }
  
  export interface PrestationFormI {
    nomPrestation: string;
    questionnaire: [];
  }

  