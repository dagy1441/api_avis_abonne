import mongoose from "mongoose";
import { MongoDataset } from "./dataset";



const prestationSchema = new mongoose.Schema
(
    { 
        nomPrestation: { type: String, required: "true"},
        questionnaire: { type: Array, default: "null"}
    },
     {timestamps: true}
);

    const prestationModel = mongoose.model('prestationDoc',prestationSchema);

    export class PrestationDataSet extends MongoDataset
    {
        static prestationDataModel = prestationModel;
        constructor()
        {
            super(); 
        }
    }