import { Router, Request, Response } from 'express';
import { routeDecorator } from '../../../core/router';
import { cdg } from '../../../utils';
import { JwtMiddleware } from '../../../utils/jwt.middlware';
//import { cdg, JwtMiddleware } from "../../../utils";
import { PrestationController } from '../http/controllers/prestation.controller';
import { PrestationI } from '../models/interface';

export class Prestation {
    app: any;
    
    constructor(app: any) { this.app = new app(); }

    getRoutes(){

      this.app.get('/prestations/addPrestationTest', (_req: Request, res: Response) => {
        return cdg.api(res, PrestationController.test());
      });

      this.app.get(
        '/prestations',
        JwtMiddleware.checkToken,
        (req: Request, res: Response) => {
        return cdg.api(res, PrestationController.getAll());
      });

      this.app.put(
        '/prestations/addprestation',
        JwtMiddleware.checkToken,
        /* ValidatorMiddleware,
        PrestationValidationMiddleware, */
        (req: Request, res: Response) => {
        let prestationData: any = req.body;
        console.log(req.user);
        delete req.user.motDePasse;
        return cdg.api(res, PrestationController.register(prestationData));
      });

      this.app.get('/prestations/activePrestation',
            (_req:Request,res:Response)=>{
                return cdg.api(res,PrestationController.getActivePrestations())
            }
        );

      // archivage d'un élément     
    this.app.put(
      '/prestations/archivePrestation/:prestationId',
      JwtMiddleware.checkToken,
      (req: Request, res: Response) => 
      {
        let Data:any = {
          status: 0
      }

        let prestationid = req.params["prestationId"]; 
        return cdg.api(res, PrestationController.archiveOnePrestation(prestationid, Data))
        
    });

    // misee à jour des données
    this.app.put(
      '/prestations/:updateprestationId',
      JwtMiddleware.checkToken,
      (req: Request, res: Response) => {
        let updateprestationId = req.params.id;
        let updateprestationNom : any = req.body;

        return cdg.api(res, PrestationController.updatePrestationById(updateprestationId, updateprestationNom ));
    });

      this.app.get(
        '/prestations/findOnePrestation/:id',
        JwtMiddleware.checkToken,
        (req: Request, res: Response) => {
          let prestationId = req.params['id'];
          console.log(prestationId);
          return cdg.api(res, PrestationController.getOnePrestationById(prestationId));
      });

    return this.app;
    }
}

const route = new Prestation(Router).getRoutes();

export class PrestationRoute {
  @routeDecorator(route)
  static router: any;
  constructor() {
    /* no need to instatiate any data */
  }
}