import { UserI } from './../../models/interfaces/user.interface';
import { LoginFormI, SaveDefaultFormI } from './../../utils/customType';

import { cdg } from '../../../../utils';
import { UserSet } from '../../models/user.model';

export class MainController {
  
  static async test() {  
    await this.save({
      email: 'test@gmail.com',
      nom: 'doee',
      prenom: 'john',
      password: 'Azerty',
      avatar: 'http://, azeaeazezae',
      status: 1,
    });
    console.log("user saved successfully");
    
    return Promise.resolve({
      error: false,
      status: 200,
      message: 'uservice up',
      data: 'version 1.0.0',
    });
  } 

  static async save(user: UserI): Promise<SaveDefaultFormI> {
    let newUser: SaveDefaultFormI = await UserSet.save(user);
    return newUser;
  }

  static async getAll() {}

  static async getOne() {}

  static async edit() {}

  static async delete() {}

  static async count() {}

  static async changePassword() {}

  static async passwordForget() {}

  static async login(loginForm: LoginFormI) {}

  static async register(userData: UserI) {
    return new Promise(async (resolve, reject) => {
      try {
        let encpass: string | boolean = await cdg.encryptPassword(
          userData.password
        );
        if (typeof encpass === 'boolean')
          return resolve({
            error: true,
            status: 500,
            message: "une erreur interne s'est produite",
            data: null,
          });
        userData.password = encpass;
        userData.avatar = '';
        userData.status = 1;
        let saveResult = await this.save(userData);
        if (saveResult.error)
          return resolve({
            error: true,
            status: 500,
            message: "une erreur interne s'est produite",
            data: saveResult,
          });
        saveResult.status = 201;
        return resolve(saveResult);
      } catch (error: any) {
        //@ts-ignore
        console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
      }
    });
  }
}
