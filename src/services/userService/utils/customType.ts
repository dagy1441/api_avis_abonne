export interface SaveDefaultFormI {
  error: boolean;
  data: any;
  message: string;
  status: number;
}

export interface LoginFormI {
  email: string;
  password: string;
}

