import { UserMiddleware } from './../http/middlwares/user.middleware';
import { ValidatorMiddleware } from './../../adminService/http/middlwares/validator.middleware';
import { Router, Request, Response } from 'express';
import { routeDecorator } from '../../../core/router';
import { cdg } from '../../../utils';
import { MainController } from '../http/controllers/main.controller';


class User {
  app: any;
  constructor(app: any) {
    this.app = new app();
  }
  
  getRoutes() {
    
    this.app.get('/test/users', (_req: Request, res: Response) => {
      return cdg.api(res, MainController.test());
    });

    this.app.put(
      '/users',
      UserMiddleware.register(),
      ValidatorMiddleware.validate,
      UserMiddleware.ExistEmail,
      (req: Request, res: Response) => {
        let userData: any = req.body;
        delete userData.confirmPassword;
        return cdg.api(res, MainController.register(userData));
      }
    );

    return this.app;
  }
}

const route = new User(Router).getRoutes();

export class UserRoute {
  @routeDecorator(route)
  static router: any;
  constructor() {
    /* no need to instatiate any data */
  }
}
