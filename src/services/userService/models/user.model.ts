import mongoose from 'mongoose';
import { DefaultDataSet } from '../../questionnaireService/models/dataset';

const userSchema = new mongoose.Schema(
  {
    nom: { type: String, default: null },
    prenom: { type: String, default: null },
    email: { required: true, type: String, unique: true },
    password: { required: true, type: String },
    avatar: { type: String },
    profil: {},
    status: { type: Number, default: 0 }, // 0: inactive, 1: active , 2: archived
  },
  { timestamps: true }
);

const UserModel = mongoose.model('userDoc', userSchema); 

export class UserSet extends DefaultDataSet {
  static defaultModel = UserModel; 
  constructor() {
    super(); 
  }
}
