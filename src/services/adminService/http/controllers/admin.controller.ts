import { Admin } from './../../routes/admin.route';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { cdg, config } from "../../../../utils";
import { AdminDataSet } from "../../models";
import { AdminI, AdminLoginFormI } from "./../../models/interface/admin.interface";
import { SaveAdminFormI } from '../../utils/customType';

export class AdminController {
  
  static async test() {
    await this.save({
      nom: "admin",
      prenom: "admin",
      email: "admin@gmail.com",
      telephone: "0758321380",
      motDePasse: "Azerty",
    });

    return Promise.resolve({
      error: false,
      status: 200,
      message: "admin service up",
      data: "version 1.0.0",
    });
  }

  static async save(admin: AdminI): Promise<SaveAdminFormI> {
    let newAdmin: SaveAdminFormI = await AdminDataSet.save(admin);
    return newAdmin;
  }

  static async login(adminData : AdminLoginFormI) : Promise<AdminLoginFormI>{
    return new Promise<any>(async (resolve, reject) => {
      try {
        
        const admin = await AdminDataSet.selectOne({ email: adminData.email });

        console.log(adminData.motDePasse);
        console.log(admin.motDePasse);
        
        const passwordIsValid = bcrypt.compareSync(adminData.motDePasse, admin.motDePasse);
        console.log(passwordIsValid);
        
        if (!admin || !passwordIsValid) 
          return resolve({
            error: true,
            status: 500,
            message: "email ou mot de passe incorrecte",
            data: null,
          });     
          
          const accessToken = jwt.sign({}, config.auth.secret!, {
            expiresIn:  '86400' //24h
          });

          return resolve({
            error: false,
            status: 200,
            message: "connectez avec succes",
            data: {"access_token":accessToken},
          });

      } catch (error) {
        console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
      }
    });
  }

  static async register(adminData: AdminI) : Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      try {
        let passwordEncrypted: string | boolean = await cdg.encryptPassword(
            adminData.motDePasse
        );
        if (typeof passwordEncrypted === "boolean")
          return resolve({
            error: true,
            status: 500,
            message: "Une erreur interne",
            data: null,
          });
        adminData.motDePasse = passwordEncrypted;
        let saveResult: any = await this.save(adminData);
        if (saveResult.error)
          return resolve({
            error: true,
            status: 500,
            message: "Une erreur interne",
            data: saveResult,
          });
        saveResult.status = 201;
        return resolve(saveResult);
      } catch (error) {
        console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
      }
    });
  }

  static async getAll(): Promise<any> {
    
    return new Promise<any>(async (resolve, reject) => {
        try {
            let allAdmin   = await AdminDataSet.select({params:null, excludes:null});

            return resolve({
              error: false,
              status: 200,
              message: "La liste des admis",
              data: allAdmin,
            });

          return resolve(allAdmin);
        } catch (error) {
            console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "une erreur interne s'est produite",
          data: null,
        });
        } 
    });
    
  }

  static async getOne() {}

  static async edit() {}

  static async delete() {}

  static async count() {}

  static async changePassword() {}

  static async passwordForget() {}
}
