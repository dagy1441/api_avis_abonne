import { use } from 'chai';
import { NextFunction, Request, Response } from 'express';
import { body } from 'express-validator';

import { AdminDataSet } from '../../models/admin.model';
import { cdg } from '../../../../utils';


export class AdminMiddleware{

    static register(){
        return[
           
            body('nom')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un nom'),

            body('prenom')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un prenom'),

            body('email')
                .isEmail()
                .withMessage('veuillez entrer un email'),
            
            body('telephone')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un numéro de téléphone'),
            
            body('telephone')
                .isLength({min: 10, max: 10})
                .withMessage('votre numéro de téléphone doit faire 10 caractères'),

            body('motDePasse')
                .isLength({min: 6})
                .withMessage('votre mot de passe doit faire au moins 6 caractère'),

            body('motDePasse')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un password'),
                
            body('confirmerMotDePasse')
                .custom((value:string, {req }) => (value === req.body.password))
                .withMessage('les mots de passes ne correspondent pas')
        ]
    }

    static login() {
        return[
            body('email').not().isEmpty().withMessage('veuillez entrer un email'),
            body('motDePasse').isLength({min: 4}).withMessage('votre mot de passe doit faire au moins 4 caractère'),
            body('motDePasse').isLength({max: 10}).withMessage('votre mot de passe doit faire au plus 10 caractère'),
            body('motDePasse').not().isEmpty().withMessage('veuillez entrer un  mot de passe')
            
        ]
    }
    
    static async ExistEmail(req: Request, res: Response, next: NextFunction){
        let email = req.body.email
        let existAdmin:boolean = await AdminDataSet.exist({email: email})
        if(existAdmin){
            let errors = [
                {
                  "msg": "Email déja utilisé",
                  "param": "email",
                  "location": "body"
                }
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
        }
        next()
    }

    static async ExistPhone(req: Request, res: Response, next: NextFunction){
        let telephone = req.body.telephone
        let existAdmin:boolean = await AdminDataSet.exist({telephone: telephone})
        if(existAdmin){
            let errors = [
                {
                  "msg": "Le numéro de téléphone déja utilisé",
                  "param": "telephone",
                  "location": "body"
                }
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
        }
        next()
    }
}