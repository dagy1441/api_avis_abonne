import { Router, Request, Response } from 'express';

import { AdminMiddleware } from './../http/middlwares/admin.middleware';
import { routeDecorator } from '../../../core/router';
import { cdg } from "../../../utils";
import { AdminController } from '../http/controllers';
import { ValidatorMiddleware } from '../http/middlwares';


export class Admin {
    app: any;
    
    constructor(app: any) { this.app = new app(); }

    getRoutes(){

        this.app.get('/demo', (_req: Request, res: Response) => {
            return cdg.api(res, AdminController.test());
        });

        this.app.put(
          '/admins/register',
          AdminMiddleware.register(),
          ValidatorMiddleware.validate,
          AdminMiddleware.ExistEmail,
          AdminMiddleware.ExistPhone,
          (req: Request, res: Response) => {
          let adminData: any = req.body;
          return cdg.api(res, AdminController.register(adminData));
      });

      this.app.put(
        '/admins/login',
        AdminMiddleware.login(),
        ValidatorMiddleware.validate,
        (req: Request, res: Response) => {
        let adminData: any = req.body;
        return cdg.api(res, AdminController.login(adminData));
    });

    this.app.get(
        '/admins',
        (req: Request, res: Response) => {
        return cdg.api(res, AdminController.getAll());
    });

    return this.app;
    }
}

const route = new Admin(Router).getRoutes();

export class AdminRoute {
  @routeDecorator(route)
  static router: any;
  constructor() {
    /* no need to instatiate any data */
  }
}