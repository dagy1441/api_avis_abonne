import mongoose from "mongoose";
import { cdg } from "../../../utils";
import { MongoDataset } from "./dataset";



const adminSchema = new mongoose.Schema(
    { 
        nom: { type: String, default: null },
        prenom: { type: String, default: null },
        email: { required: true, type: String, unique: true },
        telephone: { required: true, type: String, unique: true },
        motDePasse: { required: true, type: String }, 
    },
     {timestamps: true}
    );

    const adminModel = mongoose.model('adminDoc',adminSchema);

    export class AdminDataSet extends MongoDataset {
        static adminDataModel = adminModel;
        constructor(){
            super(); 
        }


    }
