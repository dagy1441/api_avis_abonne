export interface AdminI { 
    nom: string
    prenom: string
    email: string
    telephone: string
    motDePasse: string
}

export interface AdminLoginFormI { 
    email: string
    motDePasse: string
}

