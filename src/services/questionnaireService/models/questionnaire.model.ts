import mongoose from 'mongoose'
import { DefaultDataSet } from './dataset'

const questionnaireSchema = new mongoose.Schema({
    label: {required: true,type: String},
    questions :{type:Array,default:null},
    status : {type: Number,default:1}
},{timestamps: true})

 const QuestionnaireModel = mongoose.model('questionnaireDoc', questionnaireSchema)
 export class QuestionnaireSet extends DefaultDataSet {
    static defaultModel = QuestionnaireModel
    constructor(){
     super()   
    }
}  