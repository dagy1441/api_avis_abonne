export interface CustomResponseInterface{
    error: boolean
    data: any
    message: string
    status: number 
}
export interface QuestionnaireFormI{
    label: string
    status: number 

    questions: []
}