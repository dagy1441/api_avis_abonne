import { Router, Request, Response } from 'express';
import { routeDecorator } from '../../../core/router';
import { cdg, JwtMiddleware } from '../../../utils';
import { QuestionnaireController } from '../http/controllers';
import { ValidatorMiddleware,QuestionnaireMiddleware } from '../http/middlwares';


class Question{
    app: any;
    constructor(app: any){
        this.app = new app()   
    }
    getRoutes(){

        this.app.get('/testapi',
            (_req:Request,res:Response)=>{
                return cdg.api(res,QuestionnaireController.test())
            }
        )

        this.app.get(
            '/questionnaires',
            JwtMiddleware.checkToken,
            (_req:Request,res:Response)=>{
                return cdg.api(res,QuestionnaireController.getAll())
            }
        )

        this.app.get(
            '/questionnaires/activeQuestionnaire',
            JwtMiddleware.checkToken,
        (_req:Request,res:Response)=>{
            return cdg.api(res,QuestionnaireController.getActiveQuestionnaire())
        }
    )

        this.app.get(
            '/questionnaires/:id',
            JwtMiddleware.checkToken,
            (req:Request,res:Response)=>{
                let idQuestionaire:any = req.params.id;
                console.log(idQuestionaire);
                
                return cdg.api(res,QuestionnaireController.getOneQuestionnaireById(idQuestionaire))
            }
        )

        this.app.put(
            "/questionnaires/addQuestionnaire",
        // QuestionnaireMiddleware.VerifLabelQuestionnaire,
        // ValidatorMiddleware.validate,
        JwtMiddleware.checkToken,
            (req:Request, res:Response)=>{
                let labelData:any = req.body
                return cdg.api(res,QuestionnaireController.registerQuestionnaire(labelData))
        })
        
        this.app.put(
            "/questionnaires/updateQuestionnaire/:id",
            JwtMiddleware.checkToken,
            (req:Request, res:Response)=>{
                let Data:any = req.body
                let IdQuestionnaireUpdate = req.params.id
                return cdg.api(res,QuestionnaireController.updateOneQuestionnaire(IdQuestionnaireUpdate,Data))
        })

        this.app.put(
            "/questionnaires/archiveQuestionnaire/:id",
            JwtMiddleware.checkToken,
        (req:Request, res:Response)=>{
            let Data:any = {
                status: 0
            }
            let IdQuestionnaireUpdate = req.params.id
            return cdg.api(res,QuestionnaireController.archiveOneQuestionnaire(IdQuestionnaireUpdate,Data))
    })
         return this.app
    }
    
}
const route = new Question(Router).getRoutes()
export class QuestionnaireRoute {
    @routeDecorator(route) 
    static router: any;
    constructor() { /* no need to instatiate any data */  }
}


