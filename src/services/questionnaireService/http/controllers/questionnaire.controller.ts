import { QuestionDataSet } from './../../../questionService/models/question.model';
import { isValidObjectId } from 'mongoose'
import {QuestionnaireI} from '../../models/interface' 
import {QuestionnaireSet} from '../../models/questionnaire.model'
import { CustomResponseInterface } from '../../utils/customResponse'



export class QuestionnaireController{
   
    // static  async registerQuestionnaire(questionnaire: Questionnaire):Promise<CustomResponseInterface>{
    //     let newQuestionnaire: CustomResponseInterface = await QuestionnaireSet.save(questionnaire)
    //     return newQuestionnaire
    // }
    static async test(){
        await this.save({
            label: "questionnaire two",
            questions:[],
            status:1
        })
        return Promise.resolve({error:false, status: 200, message:"uservice up", data:'version 1.0.0'})
    } 


    static async save(questionnaire: QuestionnaireI): Promise<CustomResponseInterface>{
        let newQuestionnaire: CustomResponseInterface = await QuestionnaireSet.save(questionnaire);
        return newQuestionnaire; 
    }


    static async getAll(): Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                
                let allQuestionnaire : any = await QuestionnaireSet.select({params:null,excludes:null});
                for(const key  in allQuestionnaire) {
                    let questions = allQuestionnaire[key].questions
                    let questionList = await QuestionDataSet.select(questions);
                    allQuestionnaire[key].questions = questionList;
                }
                return resolve({error:false, status: 200, message:"la liste des questionnaires", data:allQuestionnaire});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 422, message:"une erreur interne s'est produite", data:null})

            }
        })
    }

    static async getOneQuestionnaireById(idQuestionaire: string): Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                let testId =isValidObjectId(idQuestionaire)
                let oneQuestionnaire;
                if(!testId) return resolve({error:true, status:422, message:"Questionnaire pas trouvé.", data:null});
                oneQuestionnaire = await QuestionnaireSet.selectOne({_id:idQuestionaire});
                return resolve({error:false, status: 200, message:"Questionnaire trouvé.", data:oneQuestionnaire});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

            }
        })
    }
  
static async  updateOneQuestionnaire(idQuestionaire: string,questionnaireData: QuestionnaireI):Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                let testId =isValidObjectId(idQuestionaire)
                let QuestionnaireUpdate;
                if(!testId) return resolve({error:true, status:422, message:"Le Questionnaire n'a pas été trouvé.", data:null});
                QuestionnaireUpdate = await QuestionnaireSet.update({_id:idQuestionaire},questionnaireData);
                return resolve({error:false, status:200, message:"Le Questionnaire a été modifié.", data:null});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

            }
        })
    }

    static async  archiveOneQuestionnaire(idQuestionaire: string,questionnaireData: QuestionnaireI):Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                let testId =isValidObjectId(idQuestionaire)
                let QuestionnaireArchive;
                if(!testId) return resolve({error:true, status:422, message:"Le Questionnaire n'a pas été trouvé.", data:null});
                QuestionnaireArchive = await QuestionnaireSet.update({_id:idQuestionaire},questionnaireData);
                return resolve({error:false, status:200, message:"Le Questionnaire est archivé.", data:QuestionnaireArchive});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

            }
        })
    }

    static async registerQuestionnaire(questionnaireData: QuestionnaireI):Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                let titreData = await QuestionnaireSet.selectOne({label:questionnaireData.label})
                if(titreData) return resolve({error:true,message:"le label existe déjà.",status:403,data:titreData})
                let saveResult: any = await this.save(questionnaireData);
                if(saveResult.error) 
                    return resolve({
                        error:true, 
                        status:500, 
                        message:"Une erreur interne",
                        data:saveResult
                    });

                saveResult.status = 201;
                return resolve(saveResult);
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

            }
        })
    }

    

    static async getActiveQuestionnaire(): Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                
                let allQuestionnaire = await QuestionnaireSet.select({params:{status:1},excludes:null});
                console.log(allQuestionnaire);
                return resolve({error:false, status: 200, message:"la liste des questionnaires", data:allQuestionnaire});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 422, message:"une erreur interne s'est produite", data:null})

            }
        })
    }

}