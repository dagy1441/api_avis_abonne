import { Router, Request, Response } from 'express';
import { body } from 'express-validator';
// import { cdg } from '../../../utils';
export class QuestionMiddleware{
    static checkingData(){
        return[
            body('label').not().isEmpty().withMessage('Veuillez entrer un libelle SVP'),
            body('type').isNumeric().withMessage('Veuillez entrer un nombre entier SVP'),
        ]
    }
    // static login(){
    //     return[
    //         body('login').not().isEmpty().withMessage('veuillez entrer un pseudo'),
    //         body('password').isLength({min: 4}).withMessage('votre mot de passe doit faire au moins 4 caractère'),
    //         body('password').isLength({max: 10}).withMessage('votre mot de passe doit faire au plus 10 caractère'),
    //         body('password').not().isEmpty().withMessage('veuillez entrer un password')
            
    //     ]
    // }
    // static async ExistEmail(req: Request, res: Response, next: NextFunction){
    //     let email = req.body.email
    //     let existUser:boolean = await UserSet.exist({email: email})
    //     if(existUser){
    //         let errors = [
    //             {
    //               "msg": "Email déja utilisé",
    //               "param": "email",
    //               "location": "body"
    //             }
    //           ]
    //         return cdg.api(res, Promise.resolve({
    //                 status: 422,
    //                 message: errors[0].msg,
    //                 data: errors,
    //             }));
    //     }
    //     next()
    // }
}