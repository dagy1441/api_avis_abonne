import { cdg, config } from "../../../../utils";
import { isValidObjectId } from 'mongoose'
import { QuestionDataSet } from '../../models';
import { QuestionI } from "../../models/interface/question.interface";
import { SaveQuestionFormI } from "../../utils/customType";

export class QuestionController {
  static async test() {
    await this.save({
      label: "Etes vous satisfait de la prestation reçue???",
      type: 2,
      reponses: [],
      status:1
    });

    return Promise.resolve({
      error: false,
      status: 200,
      message: "✨✌✨Question Service Up✨✌✨",
      data: "version 1.0.0",
    });
  }

  static async save(question: QuestionI): Promise<SaveQuestionFormI> {
    let newQuestion: SaveQuestionFormI = await QuestionDataSet.save(question);
    return newQuestion;
  }

  static async makeQuestion(QuestionData: QuestionI):Promise<any>{
    this.save(QuestionData);
    return Promise.resolve({
      error: false,
      status: 200,
      message: "✨✌✨Question Service Up✨✌✨",
      data: "version 1.0.0",
    });
        
}

static async getActiveQuestion(): Promise<any>{
  return new Promise<any>(async(resolve, reject)=>{
      try{
          
          let allQuestionnaire = await QuestionDataSet.select({params:{status:1},excludes:null});
          console.log(allQuestionnaire);
          return resolve({error:false, status: 200, message:"la liste des questionnaires", data:allQuestionnaire});
      }//@ts-ignore
      catch(error){
          console.warn(error)
          return reject({error:true, status: 422, message:"une erreur interne s'est produite", data:null})

      }
  })
}

  
  static async getAll(): Promise<any> {
    
    return new Promise<any>(async (resolve, reject) => {
        try {
            let allQuestion   = await QuestionDataSet.select({params:null, excludes:null});
            console.log(allQuestion);
            return resolve({
              error: false,
              status: 200,
              message: "📖📕📚 Liste des questions📖📕📚",
              data: allQuestion,
            });

          //return resolve(allQuestion);
        } catch (error) {
            console.warn(error);
        return reject({
          error: true,
          status: 500,
          message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀",
          data: null,
        });
        } 
    });
  }


  static async getOneQuestionById(idQuestion: string): Promise<any>{
    return new Promise<any>(async(resolve, reject)=>{
        try{
            let testId =isValidObjectId(idQuestion)
            let oneQuestion;
            if(!testId) return resolve({error:true, status:422, message:"Question pas trouvé.😔😔😔", data:null});
            oneQuestion = await QuestionDataSet.selectOne({_id:idQuestion});
            return resolve({error:false, status: 200, message:"Question trouvé😉✨👌.", data:oneQuestion});
        }//@ts-ignore
        catch(error){
            console.warn(error)
            return reject({error:true, status: 500, message:"💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data:null})

        }
    })
}

  static async  updateOneQuestion(idQuestion: string,QuestionData: QuestionI):Promise<any>{
    return new Promise<any>(async(resolve, reject)=>{
        try{
            let testId =isValidObjectId(idQuestion)
            let QuestionUpdate;
            if(!testId) return resolve({error:true, status:422, message:"Le Question n'a pas été trouvé.😔😔😔", data:null});
            QuestionUpdate = await QuestionDataSet.update({_id:idQuestion},QuestionData);
            return resolve({error:false, status:200, message:"La Question a été modifié.😉✨👌", data:null});
        }//@ts-ignore
        catch(error){
            console.warn(error)
            return reject({error:true, status: 500, message:"💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data:null})

        }
    })
}

static async  archiveOneQuestion(idQuestion: string,QuestionData: QuestionI):Promise<any>{
    return new Promise<any>(async(resolve, reject)=>{
        try{
            let testId =isValidObjectId(idQuestion)
            let QuestionArchive;
            if(!testId) return resolve({error:true, status:400, message:"La Question n'a pas été trouvé.😔😔😔", data:null});
            QuestionArchive = await QuestionDataSet.update({_id:idQuestion},QuestionData);
            return resolve({error:false, status:200, message:"La Question est archivée.😉✨👌", data:null});
        }//@ts-ignore
        catch(error){
            console.warn(error)
            return reject({error:true, status: 500, message:"💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data:null})

        }
    })
}

static async createQuestion(QuestionData: QuestionI):Promise<any>{
    return new Promise<any>(async(resolve, reject)=>{
        try{
          console.log("CREATION QUESTION 1.0");
          console.log(QuestionData);
            let saveResult: any = await this.save(QuestionData);
            console.log(saveResult);

            if(saveResult.error) 
                return resolve({
                    error:true, 
                    status:500, 
                    message:"💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀",
                    data:saveResult
                });

            saveResult.status = 201;
            return resolve(saveResult);
        }//@ts-ignore
        catch(error){
            console.warn(error)
            return reject({error:true, status: 500, message:"💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data:null})

        }
    })
}

  
}
