import { ValidatorMiddleware } from './../../adminService/http/middlwares/validator.middleware';
import { QuestionMiddleware } from './../http/middlwares/question.validation.middleware';
import { QuestionController } from './../http/controllers/question.controller';
import { Router, Request, Response } from 'express';
import { routeDecorator } from '../../../core/router';
import { cdg, JwtMiddleware } from '../../../utils';


class Question{
    app: any;
    constructor(app: any){
        this.app = new app()   
    }
    getRoutes(){
        this.app.get('/testapi',
            (_req:Request,res:Response)=>{
                return cdg.api(res,QuestionController.test())
            }
        )
        this.app.get(
            '/questions',
            JwtMiddleware.checkToken,
            (_req:Request,res:Response)=>{
                return cdg.api(res,QuestionController.getAll())
            }
        )
        this.app.get(
            '/questions/:id',
            JwtMiddleware.checkToken,
            (req:Request,res:Response)=>{
                let idQuestion:any = req.params["id"];
                console.log(idQuestion);
                
                return cdg.api(res,QuestionController.getOneQuestionById(idQuestion))
            }
        )
       
        this.app.put(
            "/questions/addQuestion",
            JwtMiddleware.checkToken,
            QuestionMiddleware.checkingData(),
            ValidatorMiddleware.validate,
            (req:Request, res:Response)=>{
                console.log('PREPRAT');
                let labelData:any = req.body;
                console.log(labelData);
                return cdg.api(res,QuestionController.createQuestion(labelData))
        })

        this.app.put(
            "/questions/makeQuestion",
            JwtMiddleware.checkToken,
            QuestionMiddleware.checkingData(),
        ValidatorMiddleware.validate,
        (req:Request, res:Response)=>{
            console.log('ENREGISTREMENT QUESTION');
            let qData:any = req.body
            console.log(qData);
            return cdg.api(res,QuestionController.makeQuestion(qData))
    })
        
        this.app.put("/questions/updateQuestion/:id",
        JwtMiddleware.checkToken,
        QuestionMiddleware.checkingData(),
        ValidatorMiddleware.validate,
            (req:Request, res:Response)=>{
                let Data:any = req.body
                let IdQuestionUpdate = req.params.id
                return cdg.api(res,QuestionController.updateOneQuestion(IdQuestionUpdate,Data))
        })

        this.app.put("/questions/archiveQuestion/:id",
        JwtMiddleware.checkToken,
            (req:Request, res:Response)=>{
            let Data:any = {
                status: 0
            }
            let IdQuestionUpdate = req.params.id
            return cdg.api(res,QuestionController.archiveOneQuestion(IdQuestionUpdate,Data))
    })

    this.app.get('/questions/activeQuestion',
    JwtMiddleware.checkToken,
    (_req:Request,res:Response)=>{
        return cdg.api(res,QuestionController.getActiveQuestion())
    }
)

         return this.app
    }
    
}
const route = new Question(Router).getRoutes()
export class QuestionRoute {
    @routeDecorator(route) 
    static router: any;
    constructor() { /* no need to instatiate any data */  }
}