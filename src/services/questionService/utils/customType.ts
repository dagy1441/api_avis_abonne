export interface SaveQuestionFormI{
    error: boolean;
    data: any;
    message: string;
    status: number;
}

export interface QuestionFormI{
    label: string;
    type: number;
    reponse:[];
    status: number;
}