import { MongoDataSet } from './dataset/mongo.dataset';
import mongoose from "mongoose";


const questionSchema = new mongoose.Schema(
    { 
        label: { type: String, default: null },
        type: { type: Number, default: null },
        reponses: {  type: Array, default:null},
        status: { type: Number, default:1 }
    }
    , {timestamps: true}
    );

    const questionModel = mongoose.model('questionDoc',questionSchema);

    export class QuestionDataSet extends MongoDataSet{
        static questionDataModel = questionModel;
        constructor(){
            super();
        }
    }