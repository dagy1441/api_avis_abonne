import { AgenceMiddleware } from './../http/middlwares/agence.middleware';
import { ValidatorMiddleware } from './../../adminService/http/middlwares/validator.middleware';
import { Router } from "express";
import { routeDecorator } from "../../../core/router";
import { cdg, JwtMiddleware } from "../../../utils";
import { AgenceController } from "../http/controllers";

export class Agence {
    app: any;
    
    constructor(app: any) { this.app = new app(); }

    getRoutes(){

        this.app.get('/tester', (_req: Request, res: Response) => {
            return cdg.api(res, AgenceController.test());
        });

        this.app.get(
            '/agences',
            JwtMiddleware.checkToken,
            (req: Request, res: Response) => {
            return cdg.api(res, AgenceController.getAllAgence());
        });

        this.app.put(
            '/agences/register',
            AgenceMiddleware.register(),
            ValidatorMiddleware.validate,
            AgenceMiddleware.existCodeAgence, //
            AgenceMiddleware.existNomAgence, //
            (req: Request, res: Response) => {
            let agence: any = req.body;
            return cdg.api(res, AgenceController.register(agence));
        });

        this.app.put(
            '/agences/login',
            AgenceMiddleware.login(),
            ValidatorMiddleware.validate, 
            AgenceMiddleware.isNotExistCodeAgence, //
            (req: Request, res: Response) => {
            let agence: any = req.body;
            return cdg.api(res, AgenceController.login(agence));
        });

        this.app.get('/agences/allAgencesActive',
            (_req:Request,res:Response)=>{
                return cdg.api(res,AgenceController.getActiveAgences())
            }
        )

        this.app.get(
            '/agences/:codeAgence', 
            JwtMiddleware.checkToken,
            AgenceMiddleware.isAgenceArchived,
            (req: any, res: any) => {
            let agence: any = req.params['codeAgence'];
            return cdg.api(res, AgenceController.getOneAgenceByCodeAgence(agence));
        });

        
        this.app.put(
            "/agences/archiveAgence/:id",
            JwtMiddleware.checkToken,
        (req:any, res:any)=>{
            let Data:any = {
                status: 0
            }
            let idAgenceUpdate = req.params.id
            return cdg.api(res,AgenceController.archiveAgence(idAgenceUpdate,Data))
        });

        this.app.put(
            "/agences/desarchiveAgence/:id",
            JwtMiddleware.checkToken,
        (req:any, res:any)=>{
            let Data:any = {
                status: 1
            }
            let idAgenceUpdate = req.params.id
            return cdg.api(res,AgenceController.archiveAgence(idAgenceUpdate,Data))
        }); //archiveOrDesarchiveAgence

        this.app.put(
            "/agences/archiveOrDesarchiveAgence/:codeAgence",
            JwtMiddleware.checkToken,
        (req:any, res:any)=>{
            let codeAgence = req.params.codeAgence
            return cdg.api(res,AgenceController.archiveOrDesarchiveAgence(codeAgence))
        });
        
        this.app.put(
            "/updateAgence/:id",
            JwtMiddleware.checkToken,
        (req:any, res:any)=>{
            let Data:any = req.body
            let agenceUpdate = req.params.id
            return cdg.api(res,AgenceController.onUpdateAgence(agenceUpdate,Data))
        })

        return this.app; 
    }

}

const route = new Agence(Router).getRoutes();

export class AgenceRoute {
  @routeDecorator(route)
  static router: any;
  constructor() {
    /* no need to instatiate any data */
  }
}