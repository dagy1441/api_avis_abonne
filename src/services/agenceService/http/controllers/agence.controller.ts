import { PrestationDataSet } from './../../../prestationService/models/prestation.model';
import { Prestation } from './../../../prestationService/routes/prestation.route';
import { JwtMiddleware } from './../../../../utils/jwt.middlware';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { config } from './../../../../utils/config';
import { EntityResponse } from './../../utils/customType';
import { AgenceI } from './../../models/interface/agence.interface';
import { cdg } from '../../../../utils';
import { AgenceDataSet } from '../../models/agence.model';
import { isValidObjectId } from 'mongoose';


export class AgenceController{

    static async test() {
        await this.save({
          nomAgence: "admin",
          codeAgence: "admin",
          motDePasse: "admin",
          status: 0,
        ville: { },
        commune: { },
        quartier: { },
          prestations: [],
        });
    
        return Promise.resolve({
          error: false,
          status: 200,
          message: "agence service up",
          data: "version 1.0.0",
        });
      }

      static async save(agence: AgenceI): Promise<EntityResponse> {
        let newAgence = await AgenceDataSet.save(agence);
        return newAgence;
      }

      static async register(agenceData: AgenceI): Promise<any> {

        return new Promise<any>(async (resolve, reject) => {
          try {
            let passwordEncrypted: string | boolean = await cdg.encryptPassword(
                agenceData.motDePasse
            );
            if (typeof passwordEncrypted === "boolean")
              return resolve({
                error: true,
                status: 500,
                message: "Une erreur interne",
                data: null,
              });
              let encryptedCodeAgence = cdg.generateSlug;
              agenceData.codeAgence = encryptedCodeAgence();
              agenceData.motDePasse = passwordEncrypted;
            
              let saveResult: any = await this.save(agenceData);
            
            if (saveResult.error)
              return resolve({
                error: true,
                status: 500,
                message: "Une erreur interne",
                data: saveResult,
              });
            saveResult.status = 201;
            return resolve(saveResult);
          } catch (error) {
            console.warn(error);
            return reject({
              error: true,
              status: 500,
              message: "une erreur interne s'est produite",
              data: null,
            });
          }
        });
      }

      static async login(agenceData : AgenceI) : Promise<AgenceI>{
        return new Promise<any>(async (resolve, reject) => {
          try {
            
            const agence : any = await AgenceDataSet.selectOne({ codeAgence: agenceData.codeAgence });
                console.log(agence);
                
                //const passwordIsValid = bcrypt.compareSync(agenceData.motDePasse, agence.motDePasse);
                const passwordIsValid = cdg.verifyPassword(agenceData.motDePasse, agence.motDePasse);
            console.log(passwordIsValid);
            
            if (!agence || !passwordIsValid) 
              return resolve({
                error: true,
                status: 422,
                message: "email ou mot de passe incorrect",
                data: null,
              });     
              
              const accessToken = JwtMiddleware.generateToken(agence);
    
              delete agence.motDePasse;
              
              return resolve({
                error: false,
                status: 200,
                message: "connectez avec succès !",
                data: {...agence,"access_token":accessToken},
              });
    
          } catch (error) {
            console.warn(error);
            return reject({
              error: true,
              status: 500,
              message: "une erreur interne s'est produite",
              data: null,
            });
          }
        });
      }

      static async getActiveAgences(): Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
            try{
                
                let activeAgences : any  = await AgenceDataSet.select({params:{status:1},excludes:null});
                activeAgences.forEach(element => {
                  delete element.motDePasse;
                });
                console.log(activeAgences);
                return resolve({error:false, status: 200, message:"la liste des questionnaires", data:activeAgences});
            }//@ts-ignore
            catch(error){
                console.warn(error)
                return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})
            }
        })
    }

      static async onCreateAgence(){}

      static async onUpdateAgence(idAgence : string, agenceData: AgenceI): Promise<any>{
        return new Promise<any>(async(resolve, reject)=>{
          try{
              let agenceId =isValidObjectId(idAgence)
              let agenceUpdate;
              if(!agenceId) return resolve({error:true, status:422, message:"L'agnce n'a pas été trouvé.", data:null});
              agenceUpdate = await AgenceDataSet.update({_id:idAgence},agenceData);
              return resolve({error:false, status:200, message:"Le Questionnaire a été modifié.", data:null});
          }//@ts-ignore
          catch(error){
              console.warn(error)
              return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

          }
      })
      }

      static async getAllAgence() : Promise<any>{
        return new Promise<any>(async (resolve, reject) => {
            try {
                let allAgences : any  = await AgenceDataSet.select({params:null, excludes:null});

                for(const key  in allAgences) {
                  let prestations = allAgences[key].prestations
                  let prestationList = await PrestationDataSet.select(prestations);
                  allAgences[key].prestations = prestationList;
              }

              //   for(const key  in allAgences) {
              //     let prestationId = allAgences[key].prestations[0]
              //     let prestation = await PrestationDataSet.selectOne({_id :prestationId});
              //     allAgences[key].prestations = prestation;
              // } 
                allAgences.forEach(agence => {
                  delete agence.motDePasse;
                });
                return resolve({
                  error: false,
                  status: 200,
                  message: "La liste des agences",
                  data: allAgences,
                });
    
            } catch (error) {
                console.warn(error);
            return reject({
              error: true,
              status: 500,
              message: "une erreur interne s'est produite",
              data: null,
            });
            } 
        });
      }

      static async getOneAgenceByCodeAgence(codeAgence : string) : Promise<any>{
        return new Promise<any>(async (resolve, reject) => {

          const agence : any = await AgenceDataSet.selectOne({ codeAgence: codeAgence });

          delete agence.motDePasse;
       
          if (!agence) {
            return resolve({
              error: true,
              status: 404,
              message: "Agence non trouvée",
              data: null,
            });
          }

          return resolve({
            error: false,
            status: 200,
            message: "l'agence trouvée",
            data: agence,
          });

        });
      }

      static async onDeleteAgence() : Promise<any> {}

      static async archiveAgence(idAgence : string, agenceData: AgenceI) : Promise<any> {
        return new Promise<any>(async(resolve, reject)=>{
          try{
              let agenceId = isValidObjectId(idAgence)
              let agenceArchive;
              if(!agenceId) return resolve({error:true, status:422, message:"L'agence n'a pas été trouvé.", data:null});
              agenceArchive = await AgenceDataSet.update({_id:idAgence},agenceData);
              return resolve({error:false, status:200, message:"L'agence est archivé.", data:null});
          }//@ts-ignore
          catch(error){
              console.warn(error)
              return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

          }
      })
      }

      static async archiveOrDesarchiveAgence(codeAgence : string): Promise<any> {
        return new Promise<any>(async(resolve, reject)=>{
          try{
            const agence : AgenceI = await AgenceDataSet.selectOne({ _id: codeAgence });           

            if (!agence) {
              return resolve({
                error: true,
                status: 404,
                message: "Agence non trouvée",
                data: null,
              });
            }

            console.log(agence.status);

            if(agence.status === 0){            
              agence.status = 1;
              await AgenceDataSet.update({_id:codeAgence},agence);
              //await this.onUpdateAgence(agence.codeAgence, agence); 
              console.log(agence.status);
              return resolve({error:true, status:422, message:"L'agence archivée avec succes.", data:null});
            }

            if(agence.status === 1){            
              agence.status = 0;
              await AgenceDataSet.update({_id:codeAgence},agence);
              //await this.onUpdateAgence(agence.codeAgence, agence); 
              console.log(agence.status);
              return resolve({error:true, status:422, message:"L'agence desarchivée avec succes.", data:null});
            }
            
           

          }//@ts-ignore
          catch(error){
              console.warn(error)
              return reject({error:true, status: 500, message:"une erreur interne s'est produite", data:null})

          }
      })
      }
    
}