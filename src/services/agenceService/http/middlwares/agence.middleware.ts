import { NextFunction, Request, Response } from "express";
import { body } from "express-validator";
import { cdg } from "../../../../utils";
import { AgenceDataSet } from "../../models/agence.model";

export class AgenceMiddleware {

    static register(){
        return[
           
            // body('codeAgence')
            //     .not()
            //     .isEmpty()
            //     .withMessage('veuillez entrer le code de l\'agence'),

            body('motDePasse')
                .isLength({min: 6})
                .withMessage('votre mot de passe doit faire au moins 6 caractère'),

            body('motDePasse')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer le mot de passe'),
                
            body('confirmerMotDePasse')
                .custom((value:string, {req }) => (value === req.body.password))
                .withMessage('les mots de passes ne correspondent pas')
        ]
    }

    static login() {
        return[
            body('codeAgence').not().isEmpty().withMessage('veuillez entrer un codeAgence'),
            body('motDePasse').isLength({min: 4}).withMessage('votre mot de passe doit faire au moins 4 caractère'),
            body('motDePasse').isLength({max: 10}).withMessage('votre mot de passe doit faire au plus 10 caractère'),
            body('motDePasse').not().isEmpty().withMessage('veuillez entrer un  mot de passe')    
        ]
    }

    static async existNomAgence(req: Request, res: Response, next: NextFunction){
        let nomAgence = req.body.nomAgence
        let existAgence:boolean = await AgenceDataSet.exist({nomAgence: nomAgence})
        if(existAgence){
            let errors = [
                {
                  "msg": "Agence exists deja",
                  "param": "nomAgence",
                  "location": "body"
                }
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
        }
        next()
    }

    static async existCodeAgence(req: Request, res: Response, next: NextFunction){
        let codeAgence = req.body.codeAgence
        let existAgence:boolean = await AgenceDataSet.exist({codeAgence: codeAgence})
        if(existAgence){
            let errors = [
                {
                  "msg": "Agence exists deja",
                  "param": "codeAgence",
                  "location": "body"
                }
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
        }
        next()
    }

    static async isNotExistCodeAgence(req: Request, res: Response, next: NextFunction){
        let codeAgence = req.body.codeAgence
        let existAgence:boolean = await AgenceDataSet.exist({codeAgence: codeAgence})
        if(!existAgence){
            let errors = [
                {
                  "msg": "le code agence n'exists pas",
                  "param": "codeAgence",
                  "location": "body"
                }
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
        }
        next()
    }

    static async isAgenceArchived(req: Request, res: Response, next: NextFunction){
        let codeAgence = req.params.codeAgence
        console.log(codeAgence);
        let agence: any = await AgenceDataSet.selectOne({codeAgence: codeAgence})
        let agenceArchive = agence.status;
        console.log(agenceArchive);

        if(agenceArchive===0){
            let responseYes = [
                {
                  "msg": " l'agence est déjà archivée",
                  "param": "status",
                  "location": "body"
                },
                
              ]
            return cdg.api(res, Promise.resolve({
                    status: 422,
                    message: responseYes[0].msg,
                    data: responseYes,
                }));
        }
        next()
    }

}