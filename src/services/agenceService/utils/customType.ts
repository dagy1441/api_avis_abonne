export interface EntityResponse {
  error: boolean;
  data: any;
  message: string;
  status: number;
}


