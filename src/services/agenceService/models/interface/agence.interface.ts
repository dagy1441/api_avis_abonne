export interface AgenceI {

    nomAgence: string;

    codeAgence: string;
    
    motDePasse: string;
    
    status: number;
    
    ville: { },
    
    commune: { },
    
    quartier: { },
    
    prestations: Array<any>;
    
}