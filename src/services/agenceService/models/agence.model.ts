import { cdg } from './../../../utils/coddyger';
import mongoose from "mongoose";
import { MongoDataset } from "./dataset";

const agenceSchema = new mongoose.Schema(
    { 
        nomAgence: { type: String, default: null },
        codeAgence: { type: String, default: null },
        motDePasse: { required: true, type: String }, 
        status:{  type: Number, default: 1},
        ville: { },
        commune: { },
        quartier: { },
        prestations: {type: Array, default: null},
    },
     {timestamps: true}
    );

const agenceModel = mongoose.model('agenceDoc',agenceSchema);

export class AgenceDataSet extends MongoDataset{

    static agenceDataModel = agenceModel;

    
        constructor(){
            super(); 
        }

        
        static update2(key: object, data: object) {
            return new Promise((resolve,reject) => {
                try{
                    let Q = this.agenceDataModel.findOneAndUpdate(key, data, { upsert: false });
                Q.exec();
                resolve({ status: 0, data: "Data edited successfully" });
                }catch(e){
                    console.log("erreur", e)
                    reject(e)
                }
        
                
              }).catch ((e: any) => {
                if (e) {
                    cdg.konsole(e, 1);
                    return { error: true, data: e };
                }
            });
        }

 }