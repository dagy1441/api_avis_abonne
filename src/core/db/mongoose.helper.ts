import mongoose from 'mongoose';
import { config } from '../../utils';

class MongooseHelper {
  url: string | null;
  dbname: string | null;
  port: string | number | null;
  instance: any;

  constructor(
    url: string | null,
    port: string | number | null,
    dbname: string | null
  ) {
    this.url = url;
    this.dbname = dbname;
    this.port = port;
    this.instance = null;
  }

  async getInstance() {
    if (!this.instance) {
      this.instance = await this.connect();
    }
    return this.instance;
  }

  private async connect() {
    try {
      this.instance = await mongoose.connect(
        `${this.url}:${this.port}/${this.dbname}`
      );
      console.log('Une nouvelle instance de la base de données crée');
      return this.instance;
    } catch (err) {
      console.log(err);
    }
  }
}

export const mongooseHelper = new MongooseHelper(
  config.dburl!,
  config.dbport!,
  config.dbname!
);
