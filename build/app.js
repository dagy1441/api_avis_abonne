"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("./core");
require("./utils/config");
require("./core/router/services/index");
const server = new core_1.Server;
server.handle400()
    .setRoutes()
    .handle404()
    .startServer();
