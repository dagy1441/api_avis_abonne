"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtMiddleware = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const _1 = require("./");
class JwtMiddleware {
    static checkToken(req, res, next) {
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.split(' ')[1];
            jsonwebtoken_1.default.verify(token, _1.config.jwt.secret, (err, user) => {
                if (err) {
                    const errName = err.name;
                    let errLabel;
                    if (errName === 'TokenExpiredError') {
                        errLabel = 'Votre session a expiré';
                    }
                    else if (errName === 'JsonWebTokenError') {
                        errLabel = 'Signature du token invalide';
                    }
                    else {
                        errLabel = err;
                    }
                    return _1.cdg.api(res, new Promise((resolve) => {
                        resolve({
                            status: 403,
                            message: 'error',
                            data: errLabel,
                        });
                    }));
                }
                req.user = user;
                next();
            });
        }
        else {
            return _1.cdg.api(res, new Promise((resolve) => {
                resolve({
                    status: 401,
                    message: 'error',
                    data: 'Accès non autorisée',
                });
            }));
        }
    }
    static checkAuth(req, res, next) {
        const authHeader = req.headers.authentication;
        if (authHeader) {
            const token = authHeader.split(' ')[1];
            jsonwebtoken_1.default.verify(token, _1.config.auth.secret, (err, user) => {
                if (err) {
                    let errName = err.name;
                    let errLabel;
                    if (errName === 'TokenExpiredError') {
                        errLabel = 'Votre session a expiré';
                    }
                    else if (errName === 'JsonWebTokenError') {
                        errLabel = 'Signature du token invalide';
                    }
                    else {
                        errLabel = err;
                    }
                    return _1.cdg.api(res, new Promise((resolve) => {
                        resolve({
                            status: 403,
                            message: 'error',
                            data: errLabel,
                        });
                    }));
                }
                req.user = user;
                next();
            });
        }
        else {
            return _1.cdg.api(res, new Promise((resolve) => {
                resolve({
                    status: 401,
                    message: 'error',
                    data: 'Accès API non autorisée',
                });
            }));
        }
    }
    static generateToken(data) {
        // generate an access token - exp: Math.floor(Date.now() / 1000) + (60 * 60)
        const accessToken = jsonwebtoken_1.default.sign(data, _1.config.jwt.secret, {
            expiresIn: '2440m',
        });
        return {
            accessToken: accessToken,
        };
    }
    static generateAccessToken(payloads) {
        return new Promise((resolve, reject) => {
            // generate an access token - exp: Math.floor(Date.now() / 1000) + (60 * 60)
            const accessToken = jsonwebtoken_1.default.sign({}, _1.config.auth.secret, {
                expiresIn: payloads.infinite ? '527040m' : '1440m',
            });
            resolve(accessToken);
        }).catch((e) => {
            _1.cdg.konsole(e, 1);
            return { error: true, data: "Une erreur inconnue s'est produite" };
        });
    }
    static generateRefreshToken(data) { }
}
exports.JwtMiddleware = JwtMiddleware;
