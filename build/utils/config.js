"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const envVariable = process.env;
exports.config = {
    apiPath: envVariable.API_PATH,
    dburl: envVariable.ENV === 'prod'
        ? envVariable.DB_URL_PROD
        : envVariable.DB_URL_DEV,
    dbname: envVariable.ENV === 'prod'
        ? envVariable.DB_NAME_PROD
        : envVariable.DB_NAME_DEV,
    dbport: envVariable.ENV === 'prod'
        ? envVariable.DB_PORT_PROD
        : envVariable.DB_PORT_DEV,
    appName: envVariable.API_NAME,
    serverport: envVariable.ENV === 'prod'
        ? envVariable.SERVER_PORT_DEV
        : envVariable.SERVER_PORT_PROD,
    serverhost: envVariable.ENV === 'prod'
        ? envVariable.SERVER_HOST_DEV
        : envVariable.SERVER_HOST_PROD,
    jwt: {
        secret: process.env.JWT_SECRET,
        public: process.env.JWT_PUBLIC,
    },
    auth: {
        secret: process.env.AUTH_SECRET,
        public: process.env.AUTH_PUBLIC,
        key: process.env.AUTH_KEY,
    },
};
