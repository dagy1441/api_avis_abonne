"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminDataSet = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dataset_1 = require("./dataset");
const adminSchema = new mongoose_1.default.Schema({
    nom: { type: String, default: null },
    prenom: { type: String, default: null },
    email: { required: true, type: String, unique: true },
    telephone: { required: true, type: String, unique: true },
    motDePasse: { required: true, type: String },
}, { timestamps: true });
const adminModel = mongoose_1.default.model('adminDoc', adminSchema);
class AdminDataSet extends dataset_1.MongoDataset {
    constructor() {
        super();
    }
}
exports.AdminDataSet = AdminDataSet;
AdminDataSet.adminDataModel = adminModel;
