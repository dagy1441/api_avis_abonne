"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminController = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const utils_1 = require("../../../../utils");
const models_1 = require("../../models");
class AdminController {
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                nom: "admin",
                prenom: "admin",
                email: "admin@gmail.com",
                telephone: "0758321380",
                motDePasse: "Azerty",
            });
            return Promise.resolve({
                error: false,
                status: 200,
                message: "admin service up",
                data: "version 1.0.0",
            });
        });
    }
    static save(admin) {
        return __awaiter(this, void 0, void 0, function* () {
            let newAdmin = yield models_1.AdminDataSet.save(admin);
            return newAdmin;
        });
    }
    static login(adminData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const admin = yield models_1.AdminDataSet.selectOne({ email: adminData.email });
                    console.log(adminData.motDePasse);
                    console.log(admin.motDePasse);
                    const passwordIsValid = bcrypt_1.default.compareSync(adminData.motDePasse, admin.motDePasse);
                    console.log(passwordIsValid);
                    if (!admin || !passwordIsValid)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "email ou mot de passe incorrecte",
                            data: null,
                        });
                    const accessToken = jsonwebtoken_1.default.sign({}, utils_1.config.auth.secret, {
                        expiresIn: '86400' //24h
                    });
                    return resolve({
                        error: false,
                        status: 200,
                        message: "connectez avec succes",
                        data: { "access_token": accessToken },
                    });
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static register(adminData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let passwordEncrypted = yield utils_1.cdg.encryptPassword(adminData.motDePasse);
                    if (typeof passwordEncrypted === "boolean")
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: null,
                        });
                    adminData.motDePasse = passwordEncrypted;
                    let saveResult = yield this.save(adminData);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: saveResult,
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allAdmin = yield models_1.AdminDataSet.select({ params: null, excludes: null });
                    return resolve({
                        error: false,
                        status: 200,
                        message: "La liste des admis",
                        data: allAdmin,
                    });
                    return resolve(allAdmin);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getOne() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static edit() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static delete() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static count() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static changePassword() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static passwordForget() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
}
exports.AdminController = AdminController;
