"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminMiddleware = void 0;
const express_validator_1 = require("express-validator");
const admin_model_1 = require("../../models/admin.model");
const utils_1 = require("../../../../utils");
class AdminMiddleware {
    static register() {
        return [
            (0, express_validator_1.body)('nom')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un nom'),
            (0, express_validator_1.body)('prenom')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un prenom'),
            (0, express_validator_1.body)('email')
                .isEmail()
                .withMessage('veuillez entrer un email'),
            (0, express_validator_1.body)('telephone')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un numéro de téléphone'),
            (0, express_validator_1.body)('telephone')
                .isLength({ min: 10, max: 10 })
                .withMessage('votre numéro de téléphone doit faire 10 caractères'),
            (0, express_validator_1.body)('motDePasse')
                .isLength({ min: 6 })
                .withMessage('votre mot de passe doit faire au moins 6 caractère'),
            (0, express_validator_1.body)('motDePasse')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un password'),
            (0, express_validator_1.body)('confirmerMotDePasse')
                .custom((value, { req }) => (value === req.body.password))
                .withMessage('les mots de passes ne correspondent pas')
        ];
    }
    static login() {
        return [
            (0, express_validator_1.body)('email').not().isEmpty().withMessage('veuillez entrer un email'),
            (0, express_validator_1.body)('motDePasse').isLength({ min: 4 }).withMessage('votre mot de passe doit faire au moins 4 caractère'),
            (0, express_validator_1.body)('motDePasse').isLength({ max: 10 }).withMessage('votre mot de passe doit faire au plus 10 caractère'),
            (0, express_validator_1.body)('motDePasse').not().isEmpty().withMessage('veuillez entrer un  mot de passe')
        ];
    }
    static ExistEmail(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let email = req.body.email;
            let existAdmin = yield admin_model_1.AdminDataSet.exist({ email: email });
            if (existAdmin) {
                let errors = [
                    {
                        "msg": "Email déja utilisé",
                        "param": "email",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
    static ExistPhone(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let telephone = req.body.telephone;
            let existAdmin = yield admin_model_1.AdminDataSet.exist({ telephone: telephone });
            if (existAdmin) {
                let errors = [
                    {
                        "msg": "Le numéro de téléphone déja utilisé",
                        "param": "telephone",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
}
exports.AdminMiddleware = AdminMiddleware;
