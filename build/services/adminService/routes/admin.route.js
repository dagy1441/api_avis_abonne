"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRoute = exports.Admin = void 0;
const express_1 = require("express");
const admin_middleware_1 = require("./../http/middlwares/admin.middleware");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
const controllers_1 = require("../http/controllers");
const middlwares_1 = require("../http/middlwares");
class Admin {
    constructor(app) { this.app = new app(); }
    getRoutes() {
        this.app.get('/demo', (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.AdminController.test());
        });
        this.app.put('/admins/register', admin_middleware_1.AdminMiddleware.register(), middlwares_1.ValidatorMiddleware.validate, admin_middleware_1.AdminMiddleware.ExistEmail, admin_middleware_1.AdminMiddleware.ExistPhone, (req, res) => {
            let adminData = req.body;
            return utils_1.cdg.api(res, controllers_1.AdminController.register(adminData));
        });
        this.app.put('/admins/login', admin_middleware_1.AdminMiddleware.login(), middlwares_1.ValidatorMiddleware.validate, (req, res) => {
            let adminData = req.body;
            return utils_1.cdg.api(res, controllers_1.AdminController.login(adminData));
        });
        this.app.get('/admins', (req, res) => {
            return utils_1.cdg.api(res, controllers_1.AdminController.getAll());
        });
        return this.app;
    }
}
exports.Admin = Admin;
const route = new Admin(express_1.Router).getRoutes();
class AdminRoute {
    constructor() {
        /* no need to instatiate any data */
    }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], AdminRoute, "router", void 0);
exports.AdminRoute = AdminRoute;
