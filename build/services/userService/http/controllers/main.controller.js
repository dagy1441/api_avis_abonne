"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainController = void 0;
const utils_1 = require("../../../../utils");
const user_model_1 = require("../../models/user.model");
class MainController {
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                email: 'test@gmail.com',
                nom: 'doee',
                prenom: 'john',
                password: 'Azerty',
                avatar: 'http://, azeaeazezae',
                status: 1,
            });
            console.log("user saved successfully");
            return Promise.resolve({
                error: false,
                status: 200,
                message: 'uservice up',
                data: 'version 1.0.0',
            });
        });
    }
    static save(user) {
        return __awaiter(this, void 0, void 0, function* () {
            let newUser = yield user_model_1.UserSet.save(user);
            return newUser;
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static getOne() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static edit() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static delete() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static count() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static changePassword() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static passwordForget() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static login(loginForm) {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static register(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let encpass = yield utils_1.cdg.encryptPassword(userData.password);
                    if (typeof encpass === 'boolean')
                        return resolve({
                            error: true,
                            status: 500,
                            message: "une erreur interne s'est produite",
                            data: null,
                        });
                    userData.password = encpass;
                    userData.avatar = '';
                    userData.status = 1;
                    let saveResult = yield this.save(userData);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "une erreur interne s'est produite",
                            data: saveResult,
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                }
                catch (error) {
                    //@ts-ignore
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
}
exports.MainController = MainController;
