"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoute = void 0;
const user_middleware_1 = require("./../http/middlwares/user.middleware");
const validator_middleware_1 = require("./../../adminService/http/middlwares/validator.middleware");
const express_1 = require("express");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
const main_controller_1 = require("../http/controllers/main.controller");
class User {
    constructor(app) {
        this.app = new app();
    }
    getRoutes() {
        this.app.get('/test/users', (_req, res) => {
            return utils_1.cdg.api(res, main_controller_1.MainController.test());
        });
        this.app.put('/users', user_middleware_1.UserMiddleware.register(), validator_middleware_1.ValidatorMiddleware.validate, user_middleware_1.UserMiddleware.ExistEmail, (req, res) => {
            let userData = req.body;
            delete userData.confirmPassword;
            return utils_1.cdg.api(res, main_controller_1.MainController.register(userData));
        });
        return this.app;
    }
}
const route = new User(express_1.Router).getRoutes();
class UserRoute {
    constructor() {
        /* no need to instatiate any data */
    }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], UserRoute, "router", void 0);
exports.UserRoute = UserRoute;
