"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSet = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dataset_1 = require("../../questionnaireService/models/dataset");
const userSchema = new mongoose_1.default.Schema({
    nom: { type: String, default: null },
    prenom: { type: String, default: null },
    email: { required: true, type: String, unique: true },
    password: { required: true, type: String },
    avatar: { type: String },
    profil: {},
    status: { type: Number, default: 0 }, // 0: inactive, 1: active , 2: archived
}, { timestamps: true });
const UserModel = mongoose_1.default.model('userDoc', userSchema);
class UserSet extends dataset_1.DefaultDataSet {
    constructor() {
        super();
    }
}
exports.UserSet = UserSet;
UserSet.defaultModel = UserModel;
