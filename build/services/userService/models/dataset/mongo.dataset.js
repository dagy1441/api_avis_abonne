"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoDataset = void 0;
const utils_1 = require("./../../../../utils");
class MongoDataset {
    static save(newData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    const newSave = new this.defaultModel(newData);
                    newSave.save(function (err) {
                        if (err)
                            return resolve({ error: true,
                                data: err,
                                message: "impossible de sauvegarder la donnée",
                                status: 1 });
                        return resolve({ error: false, status: 0, message: 'Donnée enregistrée avec succès!', data: null });
                    });
                }
                catch (error) {
                    console.warn(error);
                    reject({ error: true,
                        data: error,
                        message: "impossible de sauvegarder la donnée",
                        status: 1 });
                }
            });
        });
    }
    static update(key, data) {
        return new Promise((resolve, reject) => {
            try {
                let Q = this.defaultModel.findOneAndUpdate(key, data, { upsert: false });
                Q.exec();
                resolve({ status: 0, data: "Data edited successfully" });
            }
            catch (e) {
                console.log("erreur", e);
                reject(e);
            }
        }).catch((e) => {
            if (e) {
                utils_1.cdg.konsole(e, 1);
                return { error: true, data: e };
            }
        });
    }
    static remove(params) {
        return new Promise((resolve) => {
            this.defaultModel.deleteOne(params, function (err) {
                if (err)
                    resolve({ error: true, data: err, msg: '' });
                resolve({
                    status: 0,
                    data: "Data removed successfully",
                });
            }).catch((e) => {
                if (e) {
                    utils_1.cdg.konsole(e, 1);
                    return { error: true, data: e };
                }
            });
        });
    }
    static select(query) {
        return __awaiter(this, void 0, void 0, function* () {
            query.params = (utils_1.cdg.string.is_empty(query.params) ? {} : query.params);
            query.excludes = (utils_1.cdg.string.is_empty(query.excludes) ? {} : query.excludes);
            try {
                return yield new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                    let Q = yield this.defaultModel.find(query.params, query.excludes).lean();
                    resolve(Q);
                }));
            }
            catch (e) {
                if (e) {
                    utils_1.cdg.konsole(e, 1);
                    return { error: true, data: e };
                }
            }
            ;
        });
    }
    static selectOne(params) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                    resolve(yield this.defaultModel.findOne(params).select("-__v").lean());
                }));
            }
            catch (e) {
                if (e) {
                    utils_1.cdg.konsole(e, 1);
                    return { error: true, data: e };
                }
            }
        });
    }
    static exist(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let Q = yield this.defaultModel.findOne(params).select("-__v").lean();
                    if (Q == null) {
                        return resolve(false);
                    }
                    return resolve(true);
                }
                catch (e) {
                    utils_1.cdg.konsole("error exist");
                    console.log(e);
                    reject(false);
                }
            }));
        });
    }
    static rollbackSave(slug) {
        return new Promise((resolve, _reject) => {
            this.remove(slug).then((remove) => {
                if (remove.status === 1) {
                    utils_1.cdg.konsole(remove, 1);
                    resolve(false);
                }
                resolve(true);
            });
        }).catch((e) => {
            if (e) {
                utils_1.cdg.konsole(e, 1);
                return { error: true, data: e };
            }
        });
    }
    static ownData(params) {
        return __awaiter(this, void 0, void 0, function* () {
            let Q = yield this.defaultModel.findOne(params);
            return !!Q;
        });
    }
    static isContact(contact) {
        return __awaiter(this, void 0, void 0, function* () {
            let tmp = {};
            let number = contact.numero;
            let xx = yield this.checkNumberExist(number);
            if (xx === true) {
                tmp = { status: 1, data: "Ce numéro " + number + " est déjà enregistré" };
            }
            else {
                tmp = { status: 0 };
            }
            return tmp;
        });
    }
    static checkNumberExist(number) {
        return __awaiter(this, void 0, void 0, function* () {
            let Q = yield this.select({ params: { status: 'active' }, excludes: {}
            });
            let res = false;
            Q.forEach((numero) => {
                let uContacts = numero.contacts;
                for (let c in uContacts) {
                    res = uContacts[c] === number;
                    if (res === true)
                        break;
                }
            });
            return res;
        });
    }
}
exports.MongoDataset = MongoDataset;
