"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrestationDataSet = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dataset_1 = require("./dataset");
const prestationSchema = new mongoose_1.default.Schema({
    nomPrestation: { type: String, required: "true" },
    questionnaire: { type: Array, default: "null" }
}, { timestamps: true });
const prestationModel = mongoose_1.default.model('prestationDoc', prestationSchema);
class PrestationDataSet extends dataset_1.MongoDataset {
    constructor() {
        super();
    }
}
exports.PrestationDataSet = PrestationDataSet;
PrestationDataSet.prestationDataModel = prestationModel;
