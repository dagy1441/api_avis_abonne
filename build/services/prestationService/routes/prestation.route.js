"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrestationRoute = exports.Prestation = void 0;
const express_1 = require("express");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
const jwt_middlware_1 = require("../../../utils/jwt.middlware");
//import { cdg, JwtMiddleware } from "../../../utils";
const prestation_controller_1 = require("../http/controllers/prestation.controller");
class Prestation {
    constructor(app) { this.app = new app(); }
    getRoutes() {
        this.app.get('/prestations/addPrestationTest', (_req, res) => {
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.test());
        });
        this.app.get('/prestations', jwt_middlware_1.JwtMiddleware.checkToken, (req, res) => {
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.getAll());
        });
        this.app.put('/prestations/addprestation', jwt_middlware_1.JwtMiddleware.checkToken, 
        /* ValidatorMiddleware,
        PrestationValidationMiddleware, */
        (req, res) => {
            let prestationData = req.body;
            console.log(req.user);
            delete req.user.motDePasse;
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.register(prestationData));
        });
        this.app.get('/prestations/activePrestation', (_req, res) => {
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.getActivePrestations());
        });
        // archivage d'un élément     
        this.app.put('/prestations/archivePrestation/:prestationId', jwt_middlware_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = {
                status: 0
            };
            let prestationid = req.params["prestationId"];
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.archiveOnePrestation(prestationid, Data));
        });
        // misee à jour des données
        this.app.put('/prestations/:updateprestationId', jwt_middlware_1.JwtMiddleware.checkToken, (req, res) => {
            let updateprestationId = req.params.id;
            let updateprestationNom = req.body;
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.updatePrestationById(updateprestationId, updateprestationNom));
        });
        this.app.get('/prestations/findOnePrestation/:id', jwt_middlware_1.JwtMiddleware.checkToken, (req, res) => {
            let prestationId = req.params['id'];
            console.log(prestationId);
            return utils_1.cdg.api(res, prestation_controller_1.PrestationController.getOnePrestationById(prestationId));
        });
        return this.app;
    }
}
exports.Prestation = Prestation;
const route = new Prestation(express_1.Router).getRoutes();
class PrestationRoute {
    constructor() {
        /* no need to instatiate any data */
    }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], PrestationRoute, "router", void 0);
exports.PrestationRoute = PrestationRoute;
