"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrestationController = void 0;
const prestation_model_1 = require("./../../models/prestation.model");
const mongoose_1 = require("mongoose");
const questionnaire_model_1 = require("../../../questionnaireService/models/questionnaire.model");
class PrestationController {
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                nomPrestation: "Abonnement client",
                questionnaire: []
            });
            return Promise.resolve({
                error: false,
                status: 200,
                message: "admin service up",
                data: "version 1.0.0",
            });
        });
    }
    static save(prestation) {
        return __awaiter(this, void 0, void 0, function* () {
            let newPrestation = yield prestation_model_1.PrestationDataSet.save(prestation);
            return newPrestation;
        });
    }
    static getActivePrestations() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allQuestionnaire = yield prestation_model_1.PrestationDataSet.select({ params: { status: 1 }, excludes: null });
                    console.log(allQuestionnaire);
                    return resolve({ error: false, status: 200, message: "la liste des questionnaires", data: allQuestionnaire });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 422, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static updatePrestationById(PrestationId, prestationData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(PrestationId);
                    let prestationUpdate;
                    if (!testId)
                        return resolve({
                            error: true,
                            status: 422,
                            message: "La prestation n'a été trouver.",
                            data: null
                        });
                    prestationUpdate = yield prestation_model_1.PrestationDataSet.update({ _id: PrestationId }, prestationData);
                    return resolve({ error: false,
                        status: 200,
                        message: "La prestation a été modifier.",
                        data: prestationUpdate });
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    // Archivage des données
    static archiveOnePrestation(PrestationId, prestationData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(PrestationId);
                    let PrestationeArchive;
                    if (!testId)
                        return resolve({ error: true,
                            status: 400,
                            message: "La prestation n'a pas été trouvé.",
                            data: null });
                    PrestationeArchive = yield prestation_model_1.PrestationDataSet.update({ _id: PrestationId }, prestationData);
                    return resolve({ error: false,
                        status: 200, message: "La prestation est archivé.",
                        data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null });
                }
            }));
        });
    }
    static register(prestationData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let saveResult = yield this.save(prestationData);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: saveResult,
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allPrestation = yield prestation_model_1.PrestationDataSet.select({ params: null, excludes: null });
                    for (const key in allPrestation) {
                        let questionnaireId = allPrestation[key].questionnaire[0];
                        let questionnaire = yield questionnaire_model_1.QuestionnaireSet.selectOne({ _id: questionnaireId });
                        allPrestation[key].questionnaire = questionnaire;
                    }
                    return resolve({
                        error: false,
                        status: 200,
                        message: "La liste des prestaions",
                        data: allPrestation,
                    });
                    return resolve(allPrestation);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getOnePrestationById(prestationId) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let onePrestation = yield prestation_model_1.PrestationDataSet.selectOne({ prestationId: prestationId });
                    console.log(onePrestation);
                    return resolve({
                        error: false,
                        status: 201,
                        message: "La prestation recherchée",
                        data: onePrestation,
                    });
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static edit() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static delete() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static count() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
}
exports.PrestationController = PrestationController;
