"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidatorMiddleware = void 0;
const express_validator_1 = require("express-validator");
const utils_1 = require("../../../../utils");
class ValidatorMiddleware {
    static validate(req, res, next) {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return utils_1.cdg.api(res, new Promise(resolve => {
                resolve({
                    status: 422,
                    message: errors.errors[0].msg,
                    data: errors,
                });
            }));
        }
        else {
            next();
        }
    }
}
exports.ValidatorMiddleware = ValidatorMiddleware;
