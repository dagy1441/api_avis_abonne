"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrestationValidationMiddleware = void 0;
const express_validator_1 = require("express-validator");
const utils_1 = require("../../../../utils");
const prestation_model_1 = require("../../models/prestation.model");
class PrestationValidationMiddleware {
    static register() {
        return [
            (0, express_validator_1.body)('nomPrestation')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un nom'),
            (0, express_validator_1.body)('questionnaire')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer un prenom')
        ];
    }
    static login() {
        return [
            (0, express_validator_1.body)('login').not().isEmpty().withMessage('veuillez entrer un pseudo'),
            (0, express_validator_1.body)('password').isLength({ min: 4 }).withMessage('votre mot de passe doit faire au moins 4 caractère'),
            (0, express_validator_1.body)('password').isLength({ max: 10 }).withMessage('votre mot de passe doit faire au plus 10 caractère'),
            (0, express_validator_1.body)('password').not().isEmpty().withMessage('veuillez entrer un password')
        ];
    }
    static ExistEmail(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let email = req.body.email;
            let existUser = yield prestation_model_1.PrestationDataSet.exist({ email: email });
            if (existUser) {
                let errors = [
                    {
                        "msg": "Email déja utilisé",
                        "param": "email",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
}
exports.PrestationValidationMiddleware = PrestationValidationMiddleware;
