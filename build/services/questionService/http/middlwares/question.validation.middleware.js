"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionMiddleware = void 0;
const express_validator_1 = require("express-validator");
// import { cdg } from '../../../utils';
class QuestionMiddleware {
    static checkingData() {
        return [
            (0, express_validator_1.body)('label').not().isEmpty().withMessage('Veuillez entrer un libelle SVP'),
            (0, express_validator_1.body)('type').isNumeric().withMessage('Veuillez entrer un nombre entier SVP'),
        ];
    }
}
exports.QuestionMiddleware = QuestionMiddleware;
