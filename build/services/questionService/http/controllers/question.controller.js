"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionController = void 0;
const mongoose_1 = require("mongoose");
const models_1 = require("../../models");
class QuestionController {
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                label: "Etes vous satisfait de la prestation reçue???",
                type: 2,
                reponses: [],
                status: 1
            });
            return Promise.resolve({
                error: false,
                status: 200,
                message: "✨✌✨Question Service Up✨✌✨",
                data: "version 1.0.0",
            });
        });
    }
    static save(question) {
        return __awaiter(this, void 0, void 0, function* () {
            let newQuestion = yield models_1.QuestionDataSet.save(question);
            return newQuestion;
        });
    }
    static makeQuestion(QuestionData) {
        return __awaiter(this, void 0, void 0, function* () {
            this.save(QuestionData);
            return Promise.resolve({
                error: false,
                status: 200,
                message: "✨✌✨Question Service Up✨✌✨",
                data: "version 1.0.0",
            });
        });
    }
    static getActiveQuestion() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allQuestionnaire = yield models_1.QuestionDataSet.select({ params: { status: 1 }, excludes: null });
                    console.log(allQuestionnaire);
                    return resolve({ error: false, status: 200, message: "la liste des questionnaires", data: allQuestionnaire });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 422, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allQuestion = yield models_1.QuestionDataSet.select({ params: null, excludes: null });
                    console.log(allQuestion);
                    return resolve({
                        error: false,
                        status: 200,
                        message: "📖📕📚 Liste des questions📖📕📚",
                        data: allQuestion,
                    });
                    //return resolve(allQuestion);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀",
                        data: null,
                    });
                }
            }));
        });
    }
    static getOneQuestionById(idQuestion) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestion);
                    let oneQuestion;
                    if (!testId)
                        return resolve({ error: true, status: 422, message: "Question pas trouvé.😔😔😔", data: null });
                    oneQuestion = yield models_1.QuestionDataSet.selectOne({ _id: idQuestion });
                    return resolve({ error: false, status: 200, message: "Question trouvé😉✨👌.", data: oneQuestion });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data: null });
                }
            }));
        });
    }
    static updateOneQuestion(idQuestion, QuestionData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestion);
                    let QuestionUpdate;
                    if (!testId)
                        return resolve({ error: true, status: 422, message: "Le Question n'a pas été trouvé.😔😔😔", data: null });
                    QuestionUpdate = yield models_1.QuestionDataSet.update({ _id: idQuestion }, QuestionData);
                    return resolve({ error: false, status: 200, message: "La Question a été modifié.😉✨👌", data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data: null });
                }
            }));
        });
    }
    static archiveOneQuestion(idQuestion, QuestionData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestion);
                    let QuestionArchive;
                    if (!testId)
                        return resolve({ error: true, status: 400, message: "La Question n'a pas été trouvé.😔😔😔", data: null });
                    QuestionArchive = yield models_1.QuestionDataSet.update({ _id: idQuestion }, QuestionData);
                    return resolve({ error: false, status: 200, message: "La Question est archivée.😉✨👌", data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data: null });
                }
            }));
        });
    }
    static createQuestion(QuestionData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    console.log("CREATION QUESTION 1.0");
                    console.log(QuestionData);
                    let saveResult = yield this.save(QuestionData);
                    console.log(saveResult);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀",
                            data: saveResult
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "💀☠💀Une erreur interne s'est produite❗❗❗💀☠💀", data: null });
                }
            }));
        });
    }
}
exports.QuestionController = QuestionController;
