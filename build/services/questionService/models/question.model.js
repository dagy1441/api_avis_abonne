"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionDataSet = void 0;
const mongo_dataset_1 = require("./dataset/mongo.dataset");
const mongoose_1 = __importDefault(require("mongoose"));
const questionSchema = new mongoose_1.default.Schema({
    label: { type: String, default: null },
    type: { type: Number, default: null },
    reponses: { type: Array, default: null },
    status: { type: Number, default: 1 }
}, { timestamps: true });
const questionModel = mongoose_1.default.model('questionDoc', questionSchema);
class QuestionDataSet extends mongo_dataset_1.MongoDataSet {
    constructor() {
        super();
    }
}
exports.QuestionDataSet = QuestionDataSet;
QuestionDataSet.questionDataModel = questionModel;
