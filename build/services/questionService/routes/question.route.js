"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionRoute = void 0;
const validator_middleware_1 = require("./../../adminService/http/middlwares/validator.middleware");
const question_validation_middleware_1 = require("./../http/middlwares/question.validation.middleware");
const question_controller_1 = require("./../http/controllers/question.controller");
const express_1 = require("express");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
class Question {
    constructor(app) {
        this.app = new app();
    }
    getRoutes() {
        this.app.get('/testapi', (_req, res) => {
            return utils_1.cdg.api(res, question_controller_1.QuestionController.test());
        });
        this.app.get('/questions', utils_1.JwtMiddleware.checkToken, (_req, res) => {
            return utils_1.cdg.api(res, question_controller_1.QuestionController.getAll());
        });
        this.app.get('/questions/:id', utils_1.JwtMiddleware.checkToken, (req, res) => {
            let idQuestion = req.params["id"];
            console.log(idQuestion);
            return utils_1.cdg.api(res, question_controller_1.QuestionController.getOneQuestionById(idQuestion));
        });
        this.app.put("/questions/addQuestion", utils_1.JwtMiddleware.checkToken, question_validation_middleware_1.QuestionMiddleware.checkingData(), validator_middleware_1.ValidatorMiddleware.validate, (req, res) => {
            console.log('PREPRAT');
            let labelData = req.body;
            console.log(labelData);
            return utils_1.cdg.api(res, question_controller_1.QuestionController.createQuestion(labelData));
        });
        this.app.put("/questions/makeQuestion", utils_1.JwtMiddleware.checkToken, question_validation_middleware_1.QuestionMiddleware.checkingData(), validator_middleware_1.ValidatorMiddleware.validate, (req, res) => {
            console.log('ENREGISTREMENT QUESTION');
            let qData = req.body;
            console.log(qData);
            return utils_1.cdg.api(res, question_controller_1.QuestionController.makeQuestion(qData));
        });
        this.app.put("/questions/updateQuestion/:id", utils_1.JwtMiddleware.checkToken, question_validation_middleware_1.QuestionMiddleware.checkingData(), validator_middleware_1.ValidatorMiddleware.validate, (req, res) => {
            let Data = req.body;
            let IdQuestionUpdate = req.params.id;
            return utils_1.cdg.api(res, question_controller_1.QuestionController.updateOneQuestion(IdQuestionUpdate, Data));
        });
        this.app.put("/questions/archiveQuestion/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = {
                status: 0
            };
            let IdQuestionUpdate = req.params.id;
            return utils_1.cdg.api(res, question_controller_1.QuestionController.archiveOneQuestion(IdQuestionUpdate, Data));
        });
        this.app.get('/questions/activeQuestion', utils_1.JwtMiddleware.checkToken, (_req, res) => {
            return utils_1.cdg.api(res, question_controller_1.QuestionController.getActiveQuestion());
        });
        return this.app;
    }
}
const route = new Question(express_1.Router).getRoutes();
class QuestionRoute {
    constructor() { }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], QuestionRoute, "router", void 0);
exports.QuestionRoute = QuestionRoute;
