"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionnaireSet = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dataset_1 = require("./dataset");
const questionnaireSchema = new mongoose_1.default.Schema({
    label: { required: true, type: String },
    questions: { type: Array, default: null },
    status: { type: Number, default: 1 }
}, { timestamps: true });
const QuestionnaireModel = mongoose_1.default.model('questionnaireDoc', questionnaireSchema);
class QuestionnaireSet extends dataset_1.DefaultDataSet {
    constructor() {
        super();
    }
}
exports.QuestionnaireSet = QuestionnaireSet;
QuestionnaireSet.defaultModel = QuestionnaireModel;
