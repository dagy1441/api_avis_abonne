"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionnaireController = void 0;
const question_model_1 = require("./../../../questionService/models/question.model");
const mongoose_1 = require("mongoose");
const questionnaire_model_1 = require("../../models/questionnaire.model");
class QuestionnaireController {
    // static  async registerQuestionnaire(questionnaire: Questionnaire):Promise<CustomResponseInterface>{
    //     let newQuestionnaire: CustomResponseInterface = await QuestionnaireSet.save(questionnaire)
    //     return newQuestionnaire
    // }
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                label: "questionnaire two",
                questions: [],
                status: 1
            });
            return Promise.resolve({ error: false, status: 200, message: "uservice up", data: 'version 1.0.0' });
        });
    }
    static save(questionnaire) {
        return __awaiter(this, void 0, void 0, function* () {
            let newQuestionnaire = yield questionnaire_model_1.QuestionnaireSet.save(questionnaire);
            return newQuestionnaire;
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allQuestionnaire = yield questionnaire_model_1.QuestionnaireSet.select({ params: null, excludes: null });
                    for (const key in allQuestionnaire) {
                        let questions = allQuestionnaire[key].questions;
                        let questionList = yield question_model_1.QuestionDataSet.select(questions);
                        allQuestionnaire[key].questions = questionList;
                    }
                    return resolve({ error: false, status: 200, message: "la liste des questionnaires", data: allQuestionnaire });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 422, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static getOneQuestionnaireById(idQuestionaire) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestionaire);
                    let oneQuestionnaire;
                    if (!testId)
                        return resolve({ error: true, status: 422, message: "Questionnaire pas trouvé.", data: null });
                    oneQuestionnaire = yield questionnaire_model_1.QuestionnaireSet.selectOne({ _id: idQuestionaire });
                    return resolve({ error: false, status: 200, message: "Questionnaire trouvé.", data: oneQuestionnaire });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static updateOneQuestionnaire(idQuestionaire, questionnaireData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestionaire);
                    let QuestionnaireUpdate;
                    if (!testId)
                        return resolve({ error: true, status: 422, message: "Le Questionnaire n'a pas été trouvé.", data: null });
                    QuestionnaireUpdate = yield questionnaire_model_1.QuestionnaireSet.update({ _id: idQuestionaire }, questionnaireData);
                    return resolve({ error: false, status: 200, message: "Le Questionnaire a été modifié.", data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static archiveOneQuestionnaire(idQuestionaire, questionnaireData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let testId = (0, mongoose_1.isValidObjectId)(idQuestionaire);
                    let QuestionnaireArchive;
                    if (!testId)
                        return resolve({ error: true, status: 422, message: "Le Questionnaire n'a pas été trouvé.", data: null });
                    QuestionnaireArchive = yield questionnaire_model_1.QuestionnaireSet.update({ _id: idQuestionaire }, questionnaireData);
                    return resolve({ error: false, status: 200, message: "Le Questionnaire est archivé.", data: QuestionnaireArchive });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static registerQuestionnaire(questionnaireData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let titreData = yield questionnaire_model_1.QuestionnaireSet.selectOne({ label: questionnaireData.label });
                    if (titreData)
                        return resolve({ error: true, message: "le label existe déjà.", status: 403, data: titreData });
                    let saveResult = yield this.save(questionnaireData);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: saveResult
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static getActiveQuestionnaire() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allQuestionnaire = yield questionnaire_model_1.QuestionnaireSet.select({ params: { status: 1 }, excludes: null });
                    console.log(allQuestionnaire);
                    return resolve({ error: false, status: 200, message: "la liste des questionnaires", data: allQuestionnaire });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 422, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
}
exports.QuestionnaireController = QuestionnaireController;
