"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionnaireMiddleware = void 0;
const express_validator_1 = require("express-validator");
// import { cdg } from '../../../utils';
// import { UserI } from '../../models/interfaces';
// import { UserSet } from '../../models/user.model';
class QuestionnaireMiddleware {
    static VerifLabelQuestionnaire() {
        return [
            (0, express_validator_1.body)('label')
                .isEmpty()
                .not()
                .withMessage('Veuillez entrer un libelle'),
        ];
    }
}
exports.QuestionnaireMiddleware = QuestionnaireMiddleware;
