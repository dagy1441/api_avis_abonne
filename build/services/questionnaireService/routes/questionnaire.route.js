"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionnaireRoute = void 0;
const express_1 = require("express");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
const controllers_1 = require("../http/controllers");
class Question {
    constructor(app) {
        this.app = new app();
    }
    getRoutes() {
        this.app.get('/testapi', (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.test());
        });
        this.app.get('/questionnaires', utils_1.JwtMiddleware.checkToken, (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.getAll());
        });
        this.app.get('/questionnaires/activeQuestionnaire', utils_1.JwtMiddleware.checkToken, (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.getActiveQuestionnaire());
        });
        this.app.get('/questionnaires/:id', utils_1.JwtMiddleware.checkToken, (req, res) => {
            let idQuestionaire = req.params.id;
            console.log(idQuestionaire);
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.getOneQuestionnaireById(idQuestionaire));
        });
        this.app.put("/questionnaires/addQuestionnaire", 
        // QuestionnaireMiddleware.VerifLabelQuestionnaire,
        // ValidatorMiddleware.validate,
        utils_1.JwtMiddleware.checkToken, (req, res) => {
            let labelData = req.body;
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.registerQuestionnaire(labelData));
        });
        this.app.put("/questionnaires/updateQuestionnaire/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = req.body;
            let IdQuestionnaireUpdate = req.params.id;
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.updateOneQuestionnaire(IdQuestionnaireUpdate, Data));
        });
        this.app.put("/questionnaires/archiveQuestionnaire/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = {
                status: 0
            };
            let IdQuestionnaireUpdate = req.params.id;
            return utils_1.cdg.api(res, controllers_1.QuestionnaireController.archiveOneQuestionnaire(IdQuestionnaireUpdate, Data));
        });
        return this.app;
    }
}
const route = new Question(express_1.Router).getRoutes();
class QuestionnaireRoute {
    constructor() { }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], QuestionnaireRoute, "router", void 0);
exports.QuestionnaireRoute = QuestionnaireRoute;
