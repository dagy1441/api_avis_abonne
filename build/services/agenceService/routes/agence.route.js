"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AgenceRoute = exports.Agence = void 0;
const agence_middleware_1 = require("./../http/middlwares/agence.middleware");
const validator_middleware_1 = require("./../../adminService/http/middlwares/validator.middleware");
const express_1 = require("express");
const router_1 = require("../../../core/router");
const utils_1 = require("../../../utils");
const controllers_1 = require("../http/controllers");
class Agence {
    constructor(app) { this.app = new app(); }
    getRoutes() {
        this.app.get('/tester', (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.AgenceController.test());
        });
        this.app.get('/agences', utils_1.JwtMiddleware.checkToken, (req, res) => {
            return utils_1.cdg.api(res, controllers_1.AgenceController.getAllAgence());
        });
        this.app.put('/agences/register', agence_middleware_1.AgenceMiddleware.register(), validator_middleware_1.ValidatorMiddleware.validate, agence_middleware_1.AgenceMiddleware.existCodeAgence, //
        agence_middleware_1.AgenceMiddleware.existNomAgence, //
        (req, res) => {
            let agence = req.body;
            return utils_1.cdg.api(res, controllers_1.AgenceController.register(agence));
        });
        this.app.put('/agences/login', agence_middleware_1.AgenceMiddleware.login(), validator_middleware_1.ValidatorMiddleware.validate, agence_middleware_1.AgenceMiddleware.isNotExistCodeAgence, //
        (req, res) => {
            let agence = req.body;
            return utils_1.cdg.api(res, controllers_1.AgenceController.login(agence));
        });
        this.app.get('/agences/allAgencesActive', (_req, res) => {
            return utils_1.cdg.api(res, controllers_1.AgenceController.getActiveAgences());
        });
        this.app.get('/agences/:codeAgence', utils_1.JwtMiddleware.checkToken, agence_middleware_1.AgenceMiddleware.isAgenceArchived, (req, res) => {
            let agence = req.params['codeAgence'];
            return utils_1.cdg.api(res, controllers_1.AgenceController.getOneAgenceByCodeAgence(agence));
        });
        this.app.put("/agences/archiveAgence/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = {
                status: 0
            };
            let idAgenceUpdate = req.params.id;
            return utils_1.cdg.api(res, controllers_1.AgenceController.archiveAgence(idAgenceUpdate, Data));
        });
        this.app.put("/agences/desarchiveAgence/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = {
                status: 1
            };
            let idAgenceUpdate = req.params.id;
            return utils_1.cdg.api(res, controllers_1.AgenceController.archiveAgence(idAgenceUpdate, Data));
        }); //archiveOrDesarchiveAgence
        this.app.put("/agences/archiveOrDesarchiveAgence/:codeAgence", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let codeAgence = req.params.codeAgence;
            return utils_1.cdg.api(res, controllers_1.AgenceController.archiveOrDesarchiveAgence(codeAgence));
        });
        this.app.put("/updateAgence/:id", utils_1.JwtMiddleware.checkToken, (req, res) => {
            let Data = req.body;
            let agenceUpdate = req.params.id;
            return utils_1.cdg.api(res, controllers_1.AgenceController.onUpdateAgence(agenceUpdate, Data));
        });
        return this.app;
    }
}
exports.Agence = Agence;
const route = new Agence(express_1.Router).getRoutes();
class AgenceRoute {
    constructor() {
        /* no need to instatiate any data */
    }
}
__decorate([
    (0, router_1.routeDecorator)(route),
    __metadata("design:type", Object)
], AgenceRoute, "router", void 0);
exports.AgenceRoute = AgenceRoute;
