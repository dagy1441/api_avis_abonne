"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AgenceDataSet = void 0;
const coddyger_1 = require("./../../../utils/coddyger");
const mongoose_1 = __importDefault(require("mongoose"));
const dataset_1 = require("./dataset");
const agenceSchema = new mongoose_1.default.Schema({
    nomAgence: { type: String, default: null },
    codeAgence: { type: String, default: null },
    motDePasse: { required: true, type: String },
    status: { type: Number, default: 1 },
    ville: {},
    commune: {},
    quartier: {},
    prestations: { type: Array, default: null },
}, { timestamps: true });
const agenceModel = mongoose_1.default.model('agenceDoc', agenceSchema);
class AgenceDataSet extends dataset_1.MongoDataset {
    constructor() {
        super();
    }
    static update2(key, data) {
        return new Promise((resolve, reject) => {
            try {
                let Q = this.agenceDataModel.findOneAndUpdate(key, data, { upsert: false });
                Q.exec();
                resolve({ status: 0, data: "Data edited successfully" });
            }
            catch (e) {
                console.log("erreur", e);
                reject(e);
            }
        }).catch((e) => {
            if (e) {
                coddyger_1.cdg.konsole(e, 1);
                return { error: true, data: e };
            }
        });
    }
}
exports.AgenceDataSet = AgenceDataSet;
AgenceDataSet.agenceDataModel = agenceModel;
