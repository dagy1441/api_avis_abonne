"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AgenceMiddleware = void 0;
const express_validator_1 = require("express-validator");
const utils_1 = require("../../../../utils");
const agence_model_1 = require("../../models/agence.model");
class AgenceMiddleware {
    static register() {
        return [
            // body('codeAgence')
            //     .not()
            //     .isEmpty()
            //     .withMessage('veuillez entrer le code de l\'agence'),
            (0, express_validator_1.body)('motDePasse')
                .isLength({ min: 6 })
                .withMessage('votre mot de passe doit faire au moins 6 caractère'),
            (0, express_validator_1.body)('motDePasse')
                .not()
                .isEmpty()
                .withMessage('veuillez entrer le mot de passe'),
            (0, express_validator_1.body)('confirmerMotDePasse')
                .custom((value, { req }) => (value === req.body.password))
                .withMessage('les mots de passes ne correspondent pas')
        ];
    }
    static login() {
        return [
            (0, express_validator_1.body)('codeAgence').not().isEmpty().withMessage('veuillez entrer un codeAgence'),
            (0, express_validator_1.body)('motDePasse').isLength({ min: 4 }).withMessage('votre mot de passe doit faire au moins 4 caractère'),
            (0, express_validator_1.body)('motDePasse').isLength({ max: 10 }).withMessage('votre mot de passe doit faire au plus 10 caractère'),
            (0, express_validator_1.body)('motDePasse').not().isEmpty().withMessage('veuillez entrer un  mot de passe')
        ];
    }
    static existNomAgence(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let nomAgence = req.body.nomAgence;
            let existAgence = yield agence_model_1.AgenceDataSet.exist({ nomAgence: nomAgence });
            if (existAgence) {
                let errors = [
                    {
                        "msg": "Agence exists deja",
                        "param": "nomAgence",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
    static existCodeAgence(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let codeAgence = req.body.codeAgence;
            let existAgence = yield agence_model_1.AgenceDataSet.exist({ codeAgence: codeAgence });
            if (existAgence) {
                let errors = [
                    {
                        "msg": "Agence exists deja",
                        "param": "codeAgence",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
    static isNotExistCodeAgence(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let codeAgence = req.body.codeAgence;
            let existAgence = yield agence_model_1.AgenceDataSet.exist({ codeAgence: codeAgence });
            if (!existAgence) {
                let errors = [
                    {
                        "msg": "le code agence n'exists pas",
                        "param": "codeAgence",
                        "location": "body"
                    }
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: errors[0].msg,
                    data: errors,
                }));
            }
            next();
        });
    }
    static isAgenceArchived(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let codeAgence = req.params.codeAgence;
            console.log(codeAgence);
            let agence = yield agence_model_1.AgenceDataSet.selectOne({ codeAgence: codeAgence });
            let agenceArchive = agence.status;
            console.log(agenceArchive);
            if (agenceArchive === 0) {
                let responseYes = [
                    {
                        "msg": " l'agence est déjà archivée",
                        "param": "status",
                        "location": "body"
                    },
                ];
                return utils_1.cdg.api(res, Promise.resolve({
                    status: 422,
                    message: responseYes[0].msg,
                    data: responseYes,
                }));
            }
            next();
        });
    }
}
exports.AgenceMiddleware = AgenceMiddleware;
