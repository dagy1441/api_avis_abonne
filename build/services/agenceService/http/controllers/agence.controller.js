"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AgenceController = void 0;
const prestation_model_1 = require("./../../../prestationService/models/prestation.model");
const jwt_middlware_1 = require("./../../../../utils/jwt.middlware");
const utils_1 = require("../../../../utils");
const agence_model_1 = require("../../models/agence.model");
const mongoose_1 = require("mongoose");
class AgenceController {
    static test() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.save({
                nomAgence: "admin",
                codeAgence: "admin",
                motDePasse: "admin",
                status: 0,
                ville: {},
                commune: {},
                quartier: {},
                prestations: [],
            });
            return Promise.resolve({
                error: false,
                status: 200,
                message: "agence service up",
                data: "version 1.0.0",
            });
        });
    }
    static save(agence) {
        return __awaiter(this, void 0, void 0, function* () {
            let newAgence = yield agence_model_1.AgenceDataSet.save(agence);
            return newAgence;
        });
    }
    static register(agenceData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let passwordEncrypted = yield utils_1.cdg.encryptPassword(agenceData.motDePasse);
                    if (typeof passwordEncrypted === "boolean")
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: null,
                        });
                    let encryptedCodeAgence = utils_1.cdg.generateSlug;
                    agenceData.codeAgence = encryptedCodeAgence();
                    agenceData.motDePasse = passwordEncrypted;
                    let saveResult = yield this.save(agenceData);
                    if (saveResult.error)
                        return resolve({
                            error: true,
                            status: 500,
                            message: "Une erreur interne",
                            data: saveResult,
                        });
                    saveResult.status = 201;
                    return resolve(saveResult);
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static login(agenceData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const agence = yield agence_model_1.AgenceDataSet.selectOne({ codeAgence: agenceData.codeAgence });
                    console.log(agence);
                    //const passwordIsValid = bcrypt.compareSync(agenceData.motDePasse, agence.motDePasse);
                    const passwordIsValid = utils_1.cdg.verifyPassword(agenceData.motDePasse, agence.motDePasse);
                    console.log(passwordIsValid);
                    if (!agence || !passwordIsValid)
                        return resolve({
                            error: true,
                            status: 422,
                            message: "email ou mot de passe incorrect",
                            data: null,
                        });
                    const accessToken = jwt_middlware_1.JwtMiddleware.generateToken(agence);
                    delete agence.motDePasse;
                    return resolve({
                        error: false,
                        status: 200,
                        message: "connectez avec succès !",
                        data: Object.assign(Object.assign({}, agence), { "access_token": accessToken }),
                    });
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getActiveAgences() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let activeAgences = yield agence_model_1.AgenceDataSet.select({ params: { status: 1 }, excludes: null });
                    activeAgences.forEach(element => {
                        delete element.motDePasse;
                    });
                    console.log(activeAgences);
                    return resolve({ error: false, status: 200, message: "la liste des questionnaires", data: activeAgences });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static onCreateAgence() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static onUpdateAgence(idAgence, agenceData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let agenceId = (0, mongoose_1.isValidObjectId)(idAgence);
                    let agenceUpdate;
                    if (!agenceId)
                        return resolve({ error: true, status: 422, message: "L'agnce n'a pas été trouvé.", data: null });
                    agenceUpdate = yield agence_model_1.AgenceDataSet.update({ _id: idAgence }, agenceData);
                    return resolve({ error: false, status: 200, message: "Le Questionnaire a été modifié.", data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static getAllAgence() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let allAgences = yield agence_model_1.AgenceDataSet.select({ params: null, excludes: null });
                    for (const key in allAgences) {
                        let prestations = allAgences[key].prestations;
                        let prestationList = yield prestation_model_1.PrestationDataSet.select(prestations);
                        allAgences[key].prestations = prestationList;
                    }
                    //   for(const key  in allAgences) {
                    //     let prestationId = allAgences[key].prestations[0]
                    //     let prestation = await PrestationDataSet.selectOne({_id :prestationId});
                    //     allAgences[key].prestations = prestation;
                    // } 
                    allAgences.forEach(agence => {
                        delete agence.motDePasse;
                    });
                    return resolve({
                        error: false,
                        status: 200,
                        message: "La liste des agences",
                        data: allAgences,
                    });
                }
                catch (error) {
                    console.warn(error);
                    return reject({
                        error: true,
                        status: 500,
                        message: "une erreur interne s'est produite",
                        data: null,
                    });
                }
            }));
        });
    }
    static getOneAgenceByCodeAgence(codeAgence) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                const agence = yield agence_model_1.AgenceDataSet.selectOne({ codeAgence: codeAgence });
                delete agence.motDePasse;
                if (!agence) {
                    return resolve({
                        error: true,
                        status: 404,
                        message: "Agence non trouvée",
                        data: null,
                    });
                }
                return resolve({
                    error: false,
                    status: 200,
                    message: "l'agence trouvée",
                    data: agence,
                });
            }));
        });
    }
    static onDeleteAgence() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    static archiveAgence(idAgence, agenceData) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    let agenceId = (0, mongoose_1.isValidObjectId)(idAgence);
                    let agenceArchive;
                    if (!agenceId)
                        return resolve({ error: true, status: 422, message: "L'agence n'a pas été trouvé.", data: null });
                    agenceArchive = yield agence_model_1.AgenceDataSet.update({ _id: idAgence }, agenceData);
                    return resolve({ error: false, status: 200, message: "L'agence est archivé.", data: null });
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
    static archiveOrDesarchiveAgence(codeAgence) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const agence = yield agence_model_1.AgenceDataSet.selectOne({ _id: codeAgence });
                    if (!agence) {
                        return resolve({
                            error: true,
                            status: 404,
                            message: "Agence non trouvée",
                            data: null,
                        });
                    }
                    console.log(agence.status);
                    if (agence.status === 0) {
                        agence.status = 1;
                        yield agence_model_1.AgenceDataSet.update({ _id: codeAgence }, agence);
                        //await this.onUpdateAgence(agence.codeAgence, agence); 
                        console.log(agence.status);
                        return resolve({ error: true, status: 422, message: "L'agence archivée avec succes.", data: null });
                    }
                    if (agence.status === 1) {
                        agence.status = 0;
                        yield agence_model_1.AgenceDataSet.update({ _id: codeAgence }, agence);
                        //await this.onUpdateAgence(agence.codeAgence, agence); 
                        console.log(agence.status);
                        return resolve({ error: true, status: 422, message: "L'agence desarchivée avec succes.", data: null });
                    }
                } //@ts-ignore
                catch (error) {
                    console.warn(error);
                    return reject({ error: true, status: 500, message: "une erreur interne s'est produite", data: null });
                }
            }));
        });
    }
}
exports.AgenceController = AgenceController;
