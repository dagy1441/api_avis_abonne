"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mongooseHelper = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const utils_1 = require("../../utils");
class MongooseHelper {
    constructor(url, port, dbname) {
        this.url = url;
        this.dbname = dbname;
        this.port = port;
        this.instance = null;
    }
    getInstance() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.instance) {
                this.instance = yield this.connect();
            }
            return this.instance;
        });
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.instance = yield mongoose_1.default.connect(`${this.url}:${this.port}/${this.dbname}`);
                console.log('Une nouvelle instance de la base de données crée');
                return this.instance;
            }
            catch (err) {
                console.log(err);
            }
        });
    }
}
exports.mongooseHelper = new MongooseHelper(utils_1.config.dburl, utils_1.config.dbport, utils_1.config.dbname);
