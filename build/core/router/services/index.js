"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("../../../services/userService/routes");
require("../../../services/adminService/routes");
require("../../../services/agenceService/routes");
require("../../../services/questionnaireService/routes");
require("../../../services/questionService/routes");
require("../../../services/prestationService/routes");
