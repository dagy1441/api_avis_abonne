"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const utils_1 = require("../utils");
const router_1 = require("./router");
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const app = (0, express_1.default)();
class Server {
    constructor() {
        this.app = app;
        this.app.use((0, helmet_1.default)());
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.static('data/uploads'));
    }
    handle400() {
        this.app.use((err, res, next) => {
            if (err && err.status === 400 && 'body' in err)
                return res.status(400).json({ error: true, message: "Mauvaise requête", data: null });
            next();
        });
        return this;
    }
    handle404() {
        this.app.use((_err, res) => {
            return res.status(404).json({ error: true, message: "route introuvable ou inexistante", data: null });
        });
        return this;
    }
    setRoutes() {
        (0, router_1.setRoutes)(this.app);
        return this;
    }
    startServer() {
        this.app.listen(utils_1.config.serverport, () => {
            console.log(`server is running on : ${utils_1.config.serverhost}:${utils_1.config.serverport}`);
            db_1.mongooseHelper.getInstance();
        });
    }
}
exports.Server = Server;
