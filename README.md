0 -Prerequis
    installer node.js mongodb et mongo compass postman

1 - Cloner le projet
git clone https://gitlab.com/dagy1441/api_avis_abonne.git

2 - executer les commandes suivants
   npm install
   npm run dev
   npm run start

3- tester les endpoints suivants
    - creer une agence 
        put : localhost:5050/api/avis/agences/register
            { 
                "nomAgence": "agence 2",
                "motDePasse": "123456", 
                "status":1,
                "commune": { "nom" : "Macory" },
                "prestations": [
                    "6326e265744990f6be3c6b40"
                ]
            }
    - connecter une agence
        put : localhost:5050/api/avis/agences/login
            { 
                "codeAgence": "code00025555",
                "motDePasse": "123456"
            }
        
        NB : AJOUTER LE TOKEN GENERER LORS DE LA CONNECTION D4UNE AGENCE DANS LES AUTHORIZATION DANS POSTMAN
    
    - voir la liste des agences
        get : localhost:5050/api/avis/agences
    
    - voir une agence en passant son code
        get : localhost:5050/api/avis/agences/code00023

    - archiver une agence en passant son id
        put : 127.0.0.1:5050/api/avis/agences/archive/6325dddc27fd731f4171dd7d

    - desarchiver une agence en passant son id
        put : 127.0.0.1:5050/api/avis/agences/desarchive/6325dddc27fd731f4171dd7d

    - archiver ou desarchiver une agence en passant son id
        put : localhost:5050/api/avis/agences/archiveOrDesarchiveAgence/6325c59b9641137d7d138164

    
    - creer une prestations 
        put : localhost:5050/api/avis/prestations/addprestation

            {
                "nomPrestation":"abonnement client",
                "questionnaire":[
                    "6326e10605bad738f41e1148"
                ]
            }

    - voir la liste des prestations
        get : localhost:5050/api/avis/prestations

    - voir une prestations en passant son code
        get : localhost:5050/api/avis/prestations/findOnePrestation/6325b8ef3e05d524f46ac81e

    - archiver une prestations en passant son id
        get : localhost:5050/api/avis/prestations/archivePrestation/6325b8ef3e05d524f46ac81e

    - voir la liste des prestations active
        get : localhost:5050/api/avis/prestations/activePrestation

    - creer un questionnaire 
        put : 127.0.0.1:5050/api/avis/addQuestionnaire
                    {
                        "label":"questionnaire 2",
                        "questions":["6326dffa05bad738f41e1140", "6326e01c05bad738f41e1142", "6326e04105bad738f41e1144"],
                        "status":1
                    }
    - voir la liste des questionnaire
        get : 127.0.0.1:5050/api/avis/questionnaires
    - voir une questionnaire en passant son id
        get : 127.0.0.1:5050/api/avis/questionnaires/6325b93a3e05d524f46ac823
    - archiver une questionnaire en passant son id
        put : 127.0.0.1:5050/api/avis/archiveQuestionnaire/6325ad25527392953a630de0
    - voir la liste des questionnaire active
        get :  127.0.0.1:5050/api/avis/questionnaires/activeQuestionnaire

    - creer une question 
        put : localhost:5050/api/avis/questions/addQuestion
                {
                    "label":"Avez vous aimer le service ?",
                    "type": 1
                }
    - voir la liste des question
           get : 127.0.0.1:5050/api/avis/questions

    - voir une question en passant son code
            get : 127.0.0.1:5050/api/avis/questions/6325af1199b81e96cf73821d

    - archiver une question en passant son id
          put:  127.0.0.1:5050/api/avis/questions/archiveQuestion/6325af1199b81e96cf73821d
        
    - voir la liste des questions active
        get : localhost:5050/api/avis//questions/activeQuestion
